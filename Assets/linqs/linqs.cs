﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.Profiling;

public class linqs : MonoBehaviour
{

	[SerializeField]
	List<int> set1 = new List<int>() { 1, 2, 3, 4 };
	List<int> set2 = new List<int>() { 5, 6, 7 };
	List<int> set3 = new List<int>();

	[SerializeField]
	List<Avenger> AvengerRef = new List<Avenger>();


	void join()
	{
		// var NewSet = from s1 in set1 join s2 in set2 on s1 equals s2 select new {s1,s2};

		// var NewSet = from s1 in set1 join s2 in set2 select new {s1,s2};
		set3.Add(set1.ElementAt(0));


		StringBuilder s = new StringBuilder();
		foreach (var set in set3)
		{
			s.Append(set);
		}

		Debug.LogError(s);

	}


	void test()
	{
		//using Query
		// var m_groupbyList = from avg in AvengerRef group avg by avg.Age;

		//using lambda expression
		var m_groupbyList = AvengerRef.GroupBy(a => a.Age);

		StringBuilder s = new StringBuilder();
		foreach (var item in m_groupbyList)
		{

			foreach (var i in item)
			{
				s.Append(i.Name).Append(":");

			}
			s.Append(item.Key).Append(" ");

		}

		Debug.LogError(s);
	}


   void sort()
   {
	//    AvengerRef.Sort(delegate(Avenger x)
	//    {
	// 	   if(x.Age>50)
		   	
	//    }
	//    );
	//    foreach (Avenger avg in AvengerRef)
    //     {
    //         Debug.LogError(avg);
    //     }
   }

	void ValueType()
	{
		bool m_Check;
		bool m_AnotherCheck;



		m_Check = true;
		m_AnotherCheck = m_Check;
		m_AnotherCheck = false;

		Debug.LogError(m_Check);
		Debug.LogError(m_AnotherCheck);

		string m_CheckString;
		string m_AnotherCheckString;
		string m_CheckString2;


		m_CheckString = "check";
		m_AnotherCheckString = m_CheckString;
		m_CheckString2 = m_AnotherCheckString;
		m_AnotherCheckString = "another check";


		Debug.LogError(m_CheckString);
		Debug.LogError(m_AnotherCheckString);
		Debug.LogError(m_CheckString2);


		StringBuilder A1 = new StringBuilder();
		A1.Append("String 1 ");

		StringBuilder A2;
		A2 = A1;
		A2.Append("String 2");

		Debug.LogError(A1);
		Debug.LogError(A2);



		var avengerObj = new Avenger();
		avengerObj.Name = "Strange";
		Avenger AnotheravengerObj;
		AnotheravengerObj = avengerObj;
		AnotheravengerObj.Name = "Thanos";

		Debug.LogError(avengerObj.Name);
		Debug.LogError(AnotheravengerObj.Name);

	}


	// Use this for initialization
	void Start()
	{
		sort();
		// join();
		// test();
		// ValueType();

	}

	// Update is called once per frame
	void Update()
	{

	}
}

[System.Serializable]
class Avenger
{
	public string ID;
	public string Name;
	public int Age;


}
