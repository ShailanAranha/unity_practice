﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Transforms;

public class UIMainScript : MonoBehaviour
{
	[SerializeField]
	GameObject m_gotestObject = null;
	int m_intNumObjSpawn = 5;
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			InstantiateTestObject();
		}

	}
	float m_fltPosition;

	void InstantiateTestObject()
	{
		var l_entityManager = World.Active.GetOrCreateManager<EntityManager>();
		NativeArray<Entity> l_arrayObj = new NativeArray<Entity>(m_intNumObjSpawn, Allocator.Temp);
		l_entityManager.Instantiate(m_gotestObject, l_arrayObj);

		for (int i = 1; i < m_intNumObjSpawn; i++)
		{
			// l_entitymanager.AddComponentData(l_arrayObj[i], new Instantiate_ecs_Data { m_NUM = 5 });
			m_fltPosition += 10;
			float3 l_flt3Position = new float3(0, m_fltPosition, 0);
			l_entityManager.SetComponentData(l_arrayObj[i], new Position { Value = l_flt3Position });
		}


		l_arrayObj.Dispose();


	}
}
