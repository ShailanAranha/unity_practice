﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;


	[Serializable]
	public struct UIPosition : IComponentData
	{
		public float2 Position;
	}

	[UnityEngine.DisallowMultipleComponent]
	public class UIposition : ComponentDataWrapper<UIPosition>
	{


	}


// namespace Unity.Transforms
// {
//     /// <summary>
//     /// If Attached, in local space (relative to parent)
//     /// If not Attached, in world space.
//     /// </summary>
//     [Serializable]
//     public struct Position : IComponentData
//     {
//         public float3 Value;
//     }

//     [UnityEngine.DisallowMultipleComponent]
//     public class PositionComponent : ComponentDataWrapper<Position>
//     {
//     }
// }