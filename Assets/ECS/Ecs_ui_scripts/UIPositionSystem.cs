﻿using Unity.Entities;
using Unity.Jobs;
using UnityEngine;

public class UIPositionSystem : JobComponentSystem
{

	private struct UIPositionJob : IJobProcessComponentData<UIPosition>
	{
		public float x;
		public float y;

		public void Execute(ref UIPosition data)
		{
			Debug.LogError("called");
			x = data.Position.x;
			y = data.Position.y;

		}


	}


	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{

		Debug.LogError("JobHandle called");
		UIPositionJob job = new UIPositionJob
		{
			x = 5,
			y = 5
		};
		JobHandle moveHandle = job.Schedule(this, inputDeps);
		return moveHandle;

	
	}






}
