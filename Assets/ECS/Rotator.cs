﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class Rotator : MonoBehaviour
{

	public float speed = 10f;

	// void Update()
	// {
	// 			UnityEngine.Profiling.Profiler.BeginSample("rotator section update");
	// 	transform.Rotate(0.0f, speed * Time.deltaTime, 0.0f);
	// 	UnityEngine.Profiling.Profiler.EndSample();
	// }
}


class RotatorSystem : ComponentSystem
{
	struct Components
	{
		public Rotator rotator;
		public Transform transform;
	}


	protected override void OnUpdate()
	{
		float l_time = Time.deltaTime;
		UnityEngine.Profiling.Profiler.BeginSample("rotator section");

		foreach (var entities in GetEntities<Components>())
		{
			entities.transform.Rotate(0.0f, entities.rotator.speed * l_time, 0.0f);
		}

		UnityEngine.Profiling.Profiler.EndSample();


	}
}

