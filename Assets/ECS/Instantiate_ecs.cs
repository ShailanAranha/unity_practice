﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UI;

public class Instantiate_ecs : MonoBehaviour //ComponentSystem
{
	[SerializeField]
	private GameObject m_goObj = null;


	[SerializeField]
	Text m_txtObj = null;

	int m_intNumObj = 0;

	int m_intNumObjSpawn = 100;

	[SerializeField]
	ecs_data m_ecsDataComponet = null;


	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			InstantiateCubes();
		}
	}

	// protected override void OnUpdate()
	// {

	// 	if (Input.GetKeyDown(KeyCode.Space))
	// 	{
	// 		InstantiateCubes();
	// 	}

	// }


	void InstantiateCubes()
	{
		UnityEngine.Profiling.Profiler.BeginSample("InstantiateCubes");

		var l_entitymanager = World.Active.GetOrCreateManager<EntityManager>();
		NativeArray<Entity> l_arrayObj = new NativeArray<Entity>(m_intNumObjSpawn, Allocator.Temp);

		l_entitymanager.Instantiate(m_goObj, l_arrayObj);

		for (int i = 0; i < m_intNumObjSpawn; i++)
		{
			// l_entitymanager.AddComponentData(l_arrayObj[i], new Instantiate_ecs_Data { m_NUM = 5 });
			float3 l_flt3Position = new float3(UnityEngine.Random.Range(-100.0f, 100.0f), UnityEngine.Random.Range(-100.0f, 100.0f), UnityEngine.Random.Range(-100.0f, 100.0f));
			l_entitymanager.SetComponentData(l_arrayObj[i], new Position { Value = l_flt3Position });
		}

		l_arrayObj.Dispose();

		// Debug.LogError("m_num value:" + m_ecsDataComponet.);

		m_intNumObj += 100;
		m_txtObj.text = m_intNumObj.ToString();

		UnityEngine.Profiling.Profiler.EndSample();
	}


	// void test()
	// {
	// 	foreach (var entities in GetEntities<Instantiate_ecs_Data>())
	// 	{
	// 		Debug.LogError(entities.m_NUM);
	// 	}		
	// }
}
