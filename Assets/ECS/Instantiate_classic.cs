﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Instantiate_classic : MonoBehaviour
{

	[SerializeField]
	private GameObject m_goCube = null;

	[SerializeField]
	Transform m_goParent;

	[SerializeField]
	Text m_txtObj = null;

	int m_intNumObj = 0;

	

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			InstantiateCubes();
			m_intNumObj += 100;
			m_txtObj.text = m_intNumObj.ToString();
		}

	}


	void InstantiateCubes()
	{
		UnityEngine.Profiling.Profiler.BeginSample("Instantiate");
		for (int i = 0; i < 100; i++)
		{
			Instantiate(m_goCube, m_goParent);
		}
		UnityEngine.Profiling.Profiler.EndSample();
	}
}
