﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FpsCounter : MonoBehaviour
{

	public float updateInterval = 0.5F;
	private float totalFPS = 0;
	private int totalFrames = 0;
	private float timeUntilUpdate;

	[SerializeField]
	Text m_txtfps = null;

	void Start()
	{

		m_txtfps.text = "0 FPS";
		timeUntilUpdate = updateInterval;
	}

	void Update()
	{
		timeUntilUpdate -= Time.deltaTime;
		totalFPS += Time.timeScale / Time.deltaTime;
		++totalFrames;
		if (timeUntilUpdate <= 0.0)
		{
			float fps = totalFPS / totalFrames;
			m_txtfps.text = System.String.Format("{0:F2} FPS", fps);
			timeUntilUpdate = updateInterval;
			totalFPS = 0.0F;
			totalFrames = 0;
		}
	}
}
