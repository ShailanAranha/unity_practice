﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionAndFunc : MonoBehaviour {

	public InputField input;
	public Text text;
	// Use this for initialization
	void Start () {
		// for func
		// gameObject.GetComponentInChildren<Text>().text = output().ToString();

	ActionOutput();
	
	NullableInt();

	}
	
	// Update is called once per frame
	void Update () {
		predicateOutput();
		if(Input.GetKeyDown(KeyCode.N))
		{
			throw new NullReferenceException("this is working");
		}
		if(Input.GetKeyDown(KeyCode.Space))
		{
			throw new OutOfMemoryException("object out of range");
		}
	}

	// func
	// int sum(int x,int y)
	// {
	// 	return x+y;
	// }


	// int output()
	// {
	// 	Func<int,int, int> add = sum;
	// 	int result = add(10,10);
	// 	return result;
	// }


	void sum(int x, int y)
	{
		int result = x+y;
		gameObject.GetComponentInChildren<Text>().text = result.ToString();
		
	}

	IEnumerator delay()
	{			
		Action<int,int> add = sum;
		for(int i=10; i>=0; i--)
		{			
			add(i,0);
			yield return new WaitForSeconds(1);		
		}
	
	}
	void ActionOutput()
	{
		StartCoroutine(delay());
		
		// Action<int,int> add = sum;
		// add(10,10);
		// add(20,20);

	}

	//Action with lambda
	void ActionWithLambda()
	{
		 
	}

    bool UpperCaseCheck(string str)
    {
        return str.Equals(str.ToUpper());
    }

	void predicateOutput()
	{
		Predicate<string> uppercase = UpperCaseCheck;
		bool result = uppercase(input.text);
		text.text = result.ToString(); 
	}


	//Nullable values
	public Text NullCheckText = null;
	Nullable<int> NullableIntCheck= null;
	Nullable<float> nullFloatCheck= null;
    void NullableInt()
    {
        if (NullableIntCheck.HasValue)
            NullCheckText.text = NullableIntCheck.ToString();
        else
            NullCheckText.text = "null";
    }


	//tuple
	[SerializeField]
	 Tuple<int,string,string> tuplevar = new Tuple<int, string, string>(221,"sherlock","holmes");

	void tuple()
	{
	
		// tuplevar.Item1;
		int value = intarray[3,2];
	}

	int[,] intarray = new int[3,3]{{1,2,3},{10,2,3},{9,5,6}};
	

}
