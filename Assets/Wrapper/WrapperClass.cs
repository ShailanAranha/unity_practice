﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrapperClass : MonoBehaviour
{

	[SerializeField]
	bool m_IsDebug = false;

	public float CalculateSalary(float a_fltBaseSalary, float a_fltMonthlyAllowances)
	{
		if (m_IsDebug)
			Debug.Log("a_fltBaseSalary:" + a_fltBaseSalary + " a_fltMonthlyAllowances:" + a_fltMonthlyAllowances);

		float l_fltTotalSalary = a_fltBaseSalary + a_fltMonthlyAllowances;

		if (m_IsDebug)
			Debug.Log("l_fltTotalSalary:" + l_fltTotalSalary);
		WrapperClass.TaxCalculator l_wcTaxes = new WrapperClass.TaxCalculator();

		if (m_IsDebug)
			Debug.Log("Salary with tax" + l_wcTaxes.CalculateTax(l_fltTotalSalary));

		return l_wcTaxes.CalculateTax(l_fltTotalSalary);
	}

	private class TaxCalculator
	{
		public float CalculateTax(float a_fltSalary)
		{
			a_fltSalary += (a_fltSalary * 0.10f);
			return a_fltSalary;
		}
	}



	private void Start()
	{
		Debug.Log("PAY DAY CASH:" + CalculateSalary(1000.0f, 100.0f));

		// WrapperClass.TaxCalculator l_wcTaxes = new WrapperClass.TaxCalculator();
		// l_wcTaxes.CalculateTax

	}
}
