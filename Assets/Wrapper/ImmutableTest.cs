﻿using System.Collections;
using System.Collections.Generic;
using Exercises.Immutability;
using UnityEngine;

public class ImmutableTest : MonoBehaviour
{

	Dna m_DnaRef1 = null;
	Dna m_DnaRef2 = null;

	string a;


	private void Start()
	{

		m_DnaRef1 = new Dna();

		m_DnaRef1.Add(Pair.AxT);

		Debug.Log("Before Assign::m_DnaRef1:" + m_DnaRef1.Count);

		m_DnaRef2 = m_DnaRef1;

		m_DnaRef2.Add(Pair.GxC);

		Debug.Log("After Assign::m_DnaRef1:" + m_DnaRef1.Count);
		Debug.Log("After Assign::m_DnaRef1:" + m_DnaRef2.Count);

		m_DnaRef1.Add(Pair.AxT);

		Debug.Log("one more Assign::m_DnaRef1:" + m_DnaRef1.Count);

		// string a1 = "aaa";
		// string a2 = a1;
		// a2 = "qqqqqqq";
		// Debug.Log("a1:" + a1);


	}


}
