// TODO: Make Dna immutable without removing any functions.

namespace Exercises.Immutability
{
	using System.Collections.Generic;
	using System.Collections.ObjectModel;

	public enum Pair
	{
		AxT,
		GxC
	}

	public class Dna
	{
		// private readonly IList<Pair> _chain;
		private IList<Pair> _chain;

		private List<Pair> m_ChainRef = new List<Pair>();

		public IList<Pair> Chain
		{
			get { return _chain; }
		}

		public int Count
		{
			get { return _chain.Count; }
		}

		public Pair this[int index]
		{
			get { return _chain[index]; }
		}

		private static Dna s_instance = null;
		public  Dna Instance
		{
			get
			{
				if (s_instance == null)
				{
					s_instance = this;
				}

				return s_instance;
			}
		}


		public Dna()
		{
			_chain = new List<Pair>();
			// _chain = m_ChainRef;

		}

		// static Dna()
		// {
		// 	Instance._chain = new List<Pair>();
		// }


		public void Add(Pair pair)
		{

			m_ChainRef = new List<Pair>();
			m_ChainRef.Add(pair);
			_chain = m_ChainRef;

			// _chain.Add(pair);
		}

		public void Concat(Dna dna)
		{
			for (var i = 0; i < dna.Count; ++i)
				Add(dna[i]);
		}
	}
}
