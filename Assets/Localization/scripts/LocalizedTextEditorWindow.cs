﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class LocalizedTextEditorWindow : EditorWindow
{


	[MenuItem("Tool/Localized Text Editor")]
	static void Init()
	{
		EditorWindow.GetWindow(typeof(LocalizedTextEditorWindow)).Show();
	}

	public LocalizationData m_LocalizationDataRef;

	private void Start()
	{
	}

	void OnGUI()
	{

		if (m_LocalizationDataRef != null)
		{
			SerializedObject l_SerializedObject = new SerializedObject(this);
			SerializedProperty l_SerializedProperty = l_SerializedObject.FindProperty("m_LocalizationDataRef");
			EditorGUILayout.PropertyField(l_SerializedProperty, true);
			l_SerializedObject.ApplyModifiedProperties();

			if (GUILayout.Button("Save Data"))
			{
				SaveData();
			}
		}

		if (GUILayout.Button("Create new Data"))
		{
			CreateNewData();
		}


		if (GUILayout.Button("Load Data"))
		{
			LoadData();
		}

	}

	void CreateNewData()
	{
		m_LocalizationDataRef = new LocalizationData();
	}

	void SaveData()
	{
		string m_strPath = Application.streamingAssetsPath + "/Localization/scripts/StreamingAssets";

		string l_strFilePath = EditorUtility.SaveFilePanel("save localization data file", m_strPath, "", "json");

		if (!string.IsNullOrEmpty(l_strFilePath))
		{
			string l_strDataAdJson = JsonUtility.ToJson(m_LocalizationDataRef);
			File.WriteAllText(l_strFilePath, l_strDataAdJson);
		}
	}

	void LoadData()
	{
		string m_strPath = Application.streamingAssetsPath + "/Localization/scripts/StreamingAssets";

		string l_strFilePath = EditorUtility.OpenFilePanel("save localization data file", m_strPath, "json");
		if (!string.IsNullOrEmpty(l_strFilePath))
		{
			string l_strDataAsJson = File.ReadAllText(l_strFilePath);
			m_LocalizationDataRef = JsonUtility.FromJson<LocalizationData>(l_strDataAsJson); ;
		}
	}
}


//create scriptable objects for individual objects
// ScriptableObject l_outputObj = ScriptableObject.CreateInstance(typeof(LanguageData));
// LanguageData l_DataDB = (LanguageData)l_outputObj;
// string str_path = "Assets/LanguageSystem/AllLanguageResources/ResourceFiles/EnglishLanguageData.json";
// string l_strAsset = File.ReadAllText(str_path);
// // l_DataDB = JsonUtility.FromJson<LanguageDB>(l_strAsset);
// JsonUtility.FromJsonOverwrite(l_strAsset, l_DataDB);
