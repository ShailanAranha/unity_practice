﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Text;

public class StartUpManager : MonoBehaviour
{

	[SerializeField]
	Text m_txtLoading = null;
	StringBuilder LoadingText;
	string LoadingTextReset;
	[SerializeField]
	Button m_btnEnglish = null, m_btnFrench = null;

	[SerializeField]
	List<LocalizationData> data = new List<LocalizationData>();

	[SerializeField]
	GameObject m_goLoading = null;

	public const string LANG_ENGLISH = "English";
	public const string LANG_FRENCH = "French";


	private static StartUpManager s_instance = null;

	public static StartUpManager Instance
	{
		get
		{
			return s_instance;
		}
	}

	private void Awake()
	{
		if (s_instance == null)
			s_instance = this;

		m_btnEnglish.onClick.AddListener(OnClickEnglishBtn);
		m_btnFrench.onClick.AddListener(OnClickFrenchBtn);

		LoadingText = new StringBuilder();

		LoadingTextReset = "Loading";

	}

	private void Start()
	{
		// temp();
		LoadingText.Append("Loading");
		m_txtLoading.text = LoadingText.ToString();

	}
	IEnumerator LoadingMainScreen()
	{
		m_goLoading.SetActive(true);
		while (!LocalizationManager.IsReady)
		{
			LoadingText.Append(".");
			m_txtLoading.text = LoadingText.ToString();

			if (m_txtLoading.text.Equals("Loading..."))
			{
				LoadingText.Replace("...", "");
			}

			yield return null;
			// yield return new WaitForSeconds(0.5f);
		}

		System.GC.Collect();

		SceneManager.LoadScene("MainScene");

	}

	public string Language = string.Empty;

	void OnClickEnglishBtn()
	{
		LocalizationManager.SelectedLanguageId(LANG_ENGLISH);
		StartCoroutine(LoadingMainScreen());		
	}

	void OnClickFrenchBtn()
	{
		LocalizationManager.SelectedLanguageId(LANG_FRENCH);
		StartCoroutine(LoadingMainScreen());		
	}

	// Dictionary<string, string> m_dictionaryData = new Dictionary<string, string>();

	// void temp()
	// {
	// 	m_dictionaryData.Add("1", "A");
	// 	m_dictionaryData.Add("2", "B");
	// 	m_dictionaryData.Add("3", "C");
	// 	m_dictionaryData.Add("4", "D");

	// 	foreach (var item in m_dictionaryData)
	// 	{
	// 		Debug.LogError("Key:" + item.Key + " Value:" + item.Value);
	// 	}

	// }





}
