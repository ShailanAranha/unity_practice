﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

public class LanguageDb : ScriptableObject
{
	[SerializeField]
	public LocalizationData[] m_langDB;

}

public class CreateScriptabeObjectAsset : EditorWindow
{
	[MenuItem("tool/Create Language Asset")]
	static void Init()
	{
		EditorWindow.GetWindow(typeof(CreateScriptabeObjectAsset)).Show();
	}


	// [MenuItem("tool/Create Language Asset")]
	public LanguageDb Create()
	{
		LanguageDb l_asset = ScriptableObject.CreateInstance<LanguageDb>();
		string l_strPath = "Assets/Localization/scripts/StreamingAssets/Language.json";
		string l_strDataAsJson = File.ReadAllText(l_strPath);
		// Debug.LogError("l_strDataAsJson:" + l_strDataAsJson);
		// l_localizedDb = JsonUtility.FromJson<LocalizationData>(l_strDataAsJson);
		JsonUtility.FromJsonOverwrite(l_strDataAsJson, l_asset);
		AssetDatabase.CreateAsset(l_asset, "Assets/Resources/LocalizedData/LanguageDB.asset");
		AssetDatabase.SaveAssets();
		return l_asset;
	}

	public const string PASSWORDINHEX = "7500730065007200310032003300";
	public string passwordToEdit = string.Empty;
	private bool m_boolPassword;
	private bool m_boolTryAgain;

	void OnGUI()
	{
		GUI.Box(new Rect(5, 5, 270, 30), "");

		if (!m_boolPassword)
		{
			GUI.Label(new Rect(20, 12, 70, 20), "Password:");
			passwordToEdit = GUI.PasswordField(new Rect(90, 10, 90, 20), passwordToEdit, "*"[0], 10);
			if (GUI.Button(new Rect(190, 10, 50, 20), "Enter"))
			{
				if (ConvertStringToHex(passwordToEdit, Encoding.Unicode).Equals(PASSWORDINHEX))
				{
					m_boolPassword = true;
					m_boolTryAgain = false;
					passwordToEdit = "";
				}
				else
				{
					m_boolTryAgain = true;
				}

			}

		}
		else
		{
			GUI.Label(new Rect(20, 12, 170, 20), "Create Language DB Asset:");

			if (GUI.Button(new Rect(190, 10, 70, 20), "Create"))
			{
				Create();
				m_boolPassword = false;

			}

		}

		if (m_boolTryAgain)
		{
			GUI.Label(new Rect(10, 40, 170, 20), "Wrong!! Try Again");
		}

	}

	public string ConvertStringToHex(String input, System.Text.Encoding encoding)
	{
		Byte[] stringBytes = encoding.GetBytes(input);
		StringBuilder sbBytes = new StringBuilder(stringBytes.Length * 2);
		foreach (byte b in stringBytes)
		{
			sbBytes.AppendFormat("{0:X2}", b);
		}
		return sbBytes.ToString();
	}
	// public string HextoString(string InputText)
	// {

	// 	byte[] bb = Enumerable.Range(0, InputText.Length)
	// 					 .Where(x => x % 2 == 0)
	// 					 .Select(x => Convert.ToByte(InputText.Substring(x, 2), 16))
	// 					 .ToArray();
	// 	//return Convert.ToBase64String(bb);
	// 	char[] chars = new char[bb.Length / sizeof(char)];
	// 	System.Buffer.BlockCopy(bb, 0, chars, 0, bb.Length);
	// 	return new string(chars);
	// }
}
