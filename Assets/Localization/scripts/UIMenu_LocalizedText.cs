﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIMenu_LocalizedText : MonoBehaviour
{

	[SerializeField]
	Text m_txtTitle = null, m_txtWelcome = null;

	private static UIMenu_LocalizedText s_instance = null;

	private void Awake()
	{
		if (s_instance == null)
			s_instance = this;

		// switch (StartUpManager.Instance.Language)
		// {
		// 	case StartUpManager.LANG_ENGLISH:
		// 		LoadData("game_title_english", "welcome_english");
		// 		break;

		// 	case StartUpManager.LANG_FRENCH:
		// 		LoadData("game_title_french", "welcome_french");
		// 		break;

		// 	default:
		// 		LoadData("game_title_english", "welcome_english");
		// 		break;
		// }
	}

	private void Start()
	{
		LoadData();
	}


	public static void LoadData()
	{
		s_instance.m_txtTitle.text = LocalizationManager.GetLocalizedValue("game_title");
		s_instance.m_txtWelcome.text = LocalizationManager.GetLocalizedValue("welcome_msg");
	}
	
}
