﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour
{
	private Dictionary<string, string> LocalizedData;

	private static LocalizationManager s_instance = null;

	private bool m_boolIsReady;

	public static bool IsReady
	{
		get
		{
			return s_instance.m_boolIsReady;
		}
		set
		{
			value = s_instance.m_boolIsReady;
		}
	}

	private LanguageDb m_LanguageDataRef;
	private string m_strSelectedLang;



	private void ReadFromScriptableObjects()
	{
		LanguageDb l_lang = Resources.Load<LanguageDb>("LocalizedData/LanguageDB");
		m_LanguageDataRef = l_lang;
		m_boolIsReady = true;
	}


	private void Awake()
	{
		if (s_instance == null)
			s_instance = this;

		ReadFromScriptableObjects();
	}
	/* 
		public static void LoadLocalizedData(string a_strFileName)
		{
			s_instance.LocalizedData = new Dictionary<string, string>();
			string path = Application.dataPath + "/Localization/scripts/StreamingAssets";


			string l_strFilePath = Path.Combine(path, a_strFileName);

			if (File.Exists(l_strFilePath))
			{
				string l_strDataAsJson = File.ReadAllText(l_strFilePath);
				LocalizationData l_Loaddata = JsonUtility.FromJson<LocalizationData>(l_strDataAsJson);


				for (int i = 0; i < l_Loaddata.items.Length; i++)
				{
					s_instance.LocalizedData.Add(l_Loaddata.items[i].key, l_Loaddata.items[i].value);
				}

				Debug.LogError("Data loaded, dictionary contains " + s_instance.LocalizedData.Count + " entries");

				s_instance.m_boolIsReady = true;

			}

			else
			{
				Debug.LogError("File cannot be found!!!:" + l_strFilePath);
			}

		}
		public static string GetLocalizedValue(string l_strKey)
		{
			if (s_instance.LocalizedData.ContainsKey(l_strKey))
			{
				return s_instance.LocalizedData[l_strKey];
			}

			else
			{
				Debug.LogError("key does not exist!!");
			}

			return null;

		}

	*/

	public static void SelectedLanguageId(string m_langDB)
	{
		if (s_instance.m_LanguageDataRef.Contains(m_langDB))
		{
			s_instance.m_strSelectedLang = m_langDB;
			s_instance.CurrentLanguageIdData();
		}
		else
		{
			throw new System.Exception("Lang Id does not Exits!!!");
		}

	}

	int m_intCurrentLangId;

	private void CurrentLanguageIdData()
	{
		m_intCurrentLangId = m_LanguageDataRef.GetLangId(m_strSelectedLang);
	}

	public static string GetLocalizedValue(string l_strKey)
	{
		if (s_instance.m_LanguageDataRef.m_langDB[s_instance.m_intCurrentLangId].Contains(l_strKey))
		{
			return s_instance.m_LanguageDataRef.m_langDB[s_instance.m_intCurrentLangId].GetKeyValue(l_strKey);
		}
		else
		{
			Debug.LogError("key does not exist!!");
		}

		return null;

	}

}



public static class Extention
{

	#region LocalizationData
	public static bool Contains(this LocalizationData a_lang, string a_key)
	{
		int l_intCount = a_lang.items.Length;

		for (int i = 0; i < l_intCount; i++)
		{
			if (a_key.Equals(a_lang.items[i].key))
			{
				return true;
			}
		}

		return false;
	}

	public static string GetKeyValue(this LocalizationData a_lang, string a_key)
	{

		int l_intCount = a_lang.items.Length;

		for (int i = 0; i < l_intCount; i++)
		{
			if (a_key.Equals(a_lang.items[i].key))
			{
				return a_lang.items[i].value;
			}
		}

		//exception
		throw new System.Exception("Key does not Exits!! Please use the Contains check!!");
	}

	public static int Count(this LocalizationData a_lang)
	{
		return a_lang.items.Length;

	}
	#endregion

	#region Language DB

	public static bool Contains(this LanguageDb a_lang, string a_langid)
	{
		int l_intCount = a_lang.m_langDB.Length;

		for (int i = 0; i < l_intCount; i++)
		{
			if (a_langid.Equals(a_lang.m_langDB[i].LangId))
			{
				return true;
			}
		}

		return false;
	}

	public static int GetLangId(this LanguageDb a_lang, string a_key)
	{

		int l_intCount = a_lang.m_langDB.Length;

		for (int i = 0; i < l_intCount; i++)
		{
			if (a_key.Equals(a_lang.m_langDB[i].LangId))
			{
				return i;//a_lang.m_langDB[i].LangId;
			}
		}

		//exception
		throw new System.Exception("Language does not Exits!!");
	}

	public static int Count(this LanguageDb a_lang)
	{
		return a_lang.m_langDB.Length;

	}
	#endregion


}
