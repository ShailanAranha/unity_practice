﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEditor;
// using System.IO;

// [CustomEditor(typeof(LocalizationManager))]
// public class LocalizedInspectorWindow : Editor
// {

// 	LocalizationData m_LocalizationData;

// 	public override void OnInspectorGUI()
// 	{

// 		LocalizationManager myTarget = (LocalizationManager)target;

	
// 		myTarget.m_LocalizationDataRef = m_LocalizationData;

// 		if (m_LocalizationData != null)
// 		{

// 			EditorGUILayout.LabelField("Custom editor:");
// 			var serializedObject = new SerializedObject(target);
// 			var property = serializedObject.FindProperty("m_LocalizationDataRef");
// 			EditorGUILayout.PropertyField(property, true);
// 			serializedObject.ApplyModifiedProperties();

// 			if (GUILayout.Button("Save Data"))
// 			{
// 				SaveData();
// 			}
// 		}

// 		if (GUILayout.Button("Create new Data"))
// 		{
// 			CreateNewData();
// 		}


// 		if (GUILayout.Button("Load Data"))
// 		{
// 			LoadData();
// 		}

// 	}

// 	void CreateNewData()
// 	{
// 		m_LocalizationData = new LocalizationData();
// 	}

// 	void SaveData()
// 	{
// 		string m_strPath = Application.streamingAssetsPath + "/Localization/scripts/StreamingAssets";

// 		string l_strFilePath = EditorUtility.SaveFilePanel("save localization data file", m_strPath, "", "json");

// 		if (!string.IsNullOrEmpty(l_strFilePath))
// 		{
// 			string l_strDataAdJson = JsonUtility.ToJson(m_LocalizationData);
// 			File.WriteAllText(l_strFilePath, l_strDataAdJson);
// 		}
// 	}

// 	void LoadData()
// 	{
// 		string m_strPath = Application.streamingAssetsPath + "/Localization/scripts/StreamingAssets";

// 		string l_strFilePath = EditorUtility.OpenFilePanel("save localization data file", m_strPath, "json");
// 		if (!string.IsNullOrEmpty(l_strFilePath))
// 		{
// 			string l_strDataAsJson = File.ReadAllText(l_strFilePath);
// 			m_LocalizationData = JsonUtility.FromJson<LocalizationData>(l_strDataAsJson); ;
// 		}
// 	}


// }
