﻿
using System.Collections.Generic;
using UnityEngine;

// [System.Serializable]
// public class LocalizationData //: ScriptableObject
// {
// 	// public string id;

// 	// [SerializeField]/
// 	public LocalizationItem[] items;

// }


[System.Serializable]
public class LocalizationData
{
	public string LangId;
	public LocalizationItem[] items;
}


[System.Serializable]
public class LocalizationItem
{
	public string key;
	public string value;
}




