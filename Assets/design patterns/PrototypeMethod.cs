﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrototypeMethod : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{
		ConcretePrototypeA CA1 = new ConcretePrototypeA();

		CA1.Name("Iron Man");

		ConcretePrototypeA CA2, CA3;
		
		CA2 = (ConcretePrototypeA)CA1.Clone();
		CA2.Name("Thor");

		// CA3 = (ConcretePrototypeA)CA1.Clone();

	}


}

public interface IPrototype
{
	void Name(string a_name);
	IPrototype Clone();
}

public class ConcretePrototypeA : IPrototype
{

	public IPrototype Clone()
	{
		return (IPrototype)this.MemberwiseClone();
	}

	public void Name(string a_name)
	{
		Debug.LogError("Name:" + a_name);
	}
}



public class ConcretePrototypeB : IPrototype
{
	public IPrototype Clone()
	{
		return (IPrototype)this.MemberwiseClone();
	}

	public void Name(string a_name)
	{
		Debug.LogError("Name:" + a_name);
	}
}