﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class BuilderMethod : MonoBehaviour
{
	[SerializeField]
	Text m_txtCarDetails, m_txtBikeDetails;


	// Use this for initialization
	void Start()
	{

		//car
		Director_shop_honda l_objHondaCompany = new Director_shop_honda();

		IBuilder_vehical l_objHondaCar = new ConcreteBuilder_car();

		Product_vehical l_HondaCity;

		l_objHondaCompany.Construct(l_objHondaCar, "Honda-City", 584632);

		l_HondaCity = l_objHondaCar.GetResult();

		m_txtCarDetails.text = l_HondaCity.Show();

		//bike . l_HondaCBShine.

		IBuilder_vehical l_HondaCBBike = new ConcreteBuilder_bike();

		Product_vehical l_HondaCBShine;

		l_objHondaCompany.Construct(l_HondaCBBike, "Honda CB Shine", 875429);

		l_HondaCBShine = l_HondaCBBike.GetResult();

		m_txtBikeDetails.text = l_HondaCBShine.Show();




	}


}



public interface IBuilder_vehical
{
	void Unique_id(int a_no);
	void Name(string a_name);
	void Design();
	void Resources();
	Product_vehical GetResult();
}


public class ConcreteBuilder_bike : IBuilder_vehical
{
	Product_vehical m_objProduct = new Product_vehical();

	public void Design()
	{
		m_objProduct.Design("Waterfall Model Design");
	}

	public Product_vehical GetResult()
	{
		return m_objProduct;
	}

	public void Name(string a_name)
	{
		m_objProduct.ProductName(a_name);
	}

	public void Resources()
	{
		m_objProduct.Resources("2 tires");
		m_objProduct.Resources("Metal");
	}

	public void Unique_id(int a_no)
	{
		m_objProduct.ProductModelNumber(a_no);
	}
}

public class ConcreteBuilder_car : IBuilder_vehical
{
	Product_vehical m_objProduct = new Product_vehical();


	void IBuilder_vehical.Design()
	{
		m_objProduct.Design("Agile Model Design");
	}

	Product_vehical IBuilder_vehical.GetResult()
	{
		return m_objProduct;
	}

	void IBuilder_vehical.Name(string a_name)
	{
		m_objProduct.ProductName(a_name);
	}

	void IBuilder_vehical.Resources()
	{
		m_objProduct.Resources("4 tires");
		m_objProduct.Resources("Metal");
	}

	void IBuilder_vehical.Unique_id(int a_no)
	{
		m_objProduct.ProductModelNumber(a_no);
	}
}


public class Product_vehical
{
	int m_intModelNumber;
	string m_strName, m_strDesign;

	List<string> m_lstResources = new List<string>();

	public void ProductModelNumber(int a_no)
	{
		m_intModelNumber = a_no;
	}

	public void ProductName(string a_strName)
	{
		m_strName = a_strName;
	}

	public void Design(string a_strdesign)
	{
		m_strDesign = a_strdesign;
	}

	public void Resources(string a_strRes)
	{
		m_lstResources.Add(a_strRes);
	}

	StringBuilder m_sb = new StringBuilder();
	public string Show()
	{
		m_sb = m_sb.Append("Model NO: ").Append(m_intModelNumber).Append("\nName: ").Append(m_strName).Append("\nDesign method: ").Append(m_strDesign).Append("\nResources: ");

		for (int i = 0; i < m_lstResources.Count; i++)
		{
			m_sb.Append(m_lstResources[i]).Append(",");
		}

		return m_sb.ToString();

	}

}


public class Director_shop_honda
{
	public void Construct(IBuilder_vehical Builder, string a_name, int a_id)
	{
		Builder.Name(a_name);
		Builder.Unique_id(a_id);
		Builder.Design();
		Builder.Resources();
	}
}
