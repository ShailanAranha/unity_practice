﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class delegates : MonoBehaviour
{

	#region custum delegate for backup

	// delegate void RequestPreviousDataOfOldEpisodes<T, V>(T a_input, V a_output);
	// delegate void RequestPreviousDataOfOldEpisodes(Action a_input = null, Action a_output = null);

	// RequestPreviousDataOfOldEpisodes actnew = null;
	// Action<Action, Action> actio;


	#endregion

	// delegate void Multidelegates();
	// static Multidelegates mymultidelegates;


	Action mymultidelegates;


	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

		if (Input.GetKeyDown(KeyCode.Space))
		{

			mymultidelegates += drop;
			mymultidelegates += fieldOfView;
			//mymultidelegates += respawning;


			if (mymultidelegates != null)
			{
				mymultidelegates();
			}
		}

		//using anonymous method
		if (Input.GetKeyDown(KeyCode.A))
		{

			mymultidelegates += () =>
			{
				gameObject.AddComponent<Rigidbody>();
			};

			mymultidelegates += () =>
			{
				Camera.main.fieldOfView = 40.0f;
			};
			// mymultidelegates += fieldOfView;
			//mymultidelegates += respawning;


			if (mymultidelegates != null)
			{
				mymultidelegates();
			}
		}


		//by passing method
		if (Input.GetKeyDown(KeyCode.M))
		{

			mainDelegate(drop);
			mainDelegate(fieldOfView);



			if (mymultidelegates != null)
			{
				mymultidelegates();
			}

		}

	}


	void mainDelegate(Action a_mymultidelegates)
	{
		mymultidelegates += a_mymultidelegates;
	}


	void drop()
	{
		gameObject.AddComponent<Rigidbody>();
	}

	void fieldOfView()
	{
		Camera.main.fieldOfView = 40.0f;
	}

	//	void re()
	//	{
	//		
	//
	//	}
}
