﻿
using UnityEngine;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MeshFormation : MonoBehaviour
{

	private void Start()
	{
		CreateCube();
	}
	void CreateCube()
	{
		// define vertices
		Vector3[] l_cubeVertices = {new Vector3(0,0,0),new Vector3(1,0,0),new Vector3(1,1,0), new Vector3(0,1,0),
												new Vector3(0,1,1),new Vector3(1,1,1),new Vector3(1,0,1),new Vector3(0,0,1)};


		//define triangles
		/*
		Note: The sequence of the points for the triange should be in clockwise, as the face that contains clockwise direction will be rendered
		 */

		 //8 vertices (difficult to map UVs, thats why unity uses 24 vertices)
		int[] l_cubeTriangles =
		 {
			 //front face 
			 0,2,1,
			 0,3,2,

			 //top face
			3,5,2,
			3,4,5,

			//back face
			5,4,7,
			5,7,6,

			//bottom face
			0,1,6,
			0,6,7,

			//right face
			1,5,6,
			1,2,5,

			//left face
			0,4,3,
			0,7,4

		 };

		//Assign UVS
		// Vector2[] l_cubeUVs = new Vector2[]
		// {
		// 	new Vector2(0.5f,0.5f),
		// 	new Vector2(0.5f,0.25f),
		// 	new Vector2(0.75f,0.25f),
		// 	new Vector2(0.75f,0.5f),

		// 	new Vector2(1f,0.5f),
		// 	new Vector2(1f,0.25f),
		// 	new Vector2(0.25f,0.5f),
		// 	new Vector2(0.25f,0.25f),



		// };

		// Vector2[] l_cubeUVs = new Vector2[l_cubeVertices.Length];

        // for (int i = 0; i < l_cubeUVs.Length; i++)
        // {
        //     l_cubeUVs[i] = new Vector2(l_cubeVertices[i].x, l_cubeVertices[i].z);
        // }


		Mesh l_mesh = GetComponent<MeshFilter>().mesh;
		l_mesh.Clear();
		l_mesh.vertices = l_cubeVertices;
		l_mesh.triangles = l_cubeTriangles;

		// l_mesh.uv = l_cubeUVs;
		l_mesh.RecalculateNormals();


		// MeshRenderer l_mr = GetComponent<MeshRenderer>();
		// l_mr.material.SetTexture

	}

}
