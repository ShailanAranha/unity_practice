﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class terrain_generation : MonoBehaviour
{
	Mesh m_mesh;

	Vector3[] m_Vectices;

	int[] m_triangles;

	[SerializeField]
	int m_intXsize = 20, m_intZsize = 20;

	private void Start()
	{
		m_mesh = GetComponent<MeshFilter>().mesh;
		CreateMesh();
		UpdateMesh();
		// CreateMesh();
	}

	// private void Update()
	// {
	// 	// CreateMesh
	// 	// UpdateMesh();
	// }

	void CreateMesh()
	{
		m_Vectices = new Vector3[(m_intXsize + 1) * (m_intZsize + 1)];

		for (int i = 0, z_index = 0; z_index <= m_intZsize; z_index++)
		{
			for (int x_index = 0; x_index <= m_intXsize; x_index++)
			{
				float l_y = Mathf.PerlinNoise(x_index * 0.3f, z_index * 0.3f) * 2f;
				m_Vectices[i] = new Vector3(x_index, l_y, z_index);
				i++;
			}
		}

		m_triangles = new int[m_intXsize * m_intZsize * 6];
		int l_vert = 0, l_tri = 0;

		for (int i = 0; i < m_intZsize; i++)
		{

			for (int x = 0; x < m_intXsize; x++)
			{
				m_triangles[l_tri + 0] = l_vert + 0;
				m_triangles[l_tri + 1] = l_vert + m_intXsize + 1;
				m_triangles[l_tri + 2] = l_vert + 1;
				m_triangles[l_tri + 3] = l_vert + 1;
				m_triangles[l_tri + 4] = l_vert + m_intXsize + 1;
				m_triangles[l_tri + 5] = l_vert + m_intXsize + 2;

				l_vert++;
				l_tri += 6;

				// yield return new WaitForSeconds(0.1f);
			}

			l_vert++;
		}





	}

	void UpdateMesh()
	{
		m_mesh.Clear();

		m_mesh.vertices = m_Vectices;
		m_mesh.triangles = m_triangles;

		m_mesh.RecalculateNormals();
	}

	private void OnDrawGizmos()
	{
		if (m_Vectices == null)
		{
			return;
		}

		for (int i_index = 0; i_index < m_Vectices.Length; i_index++)
		{
			Gizmos.DrawSphere(m_Vectices[i_index], 0.1f);
		}
	}
}
