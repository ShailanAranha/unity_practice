﻿
using UnityEngine;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class DiceCreation : MonoBehaviour
{
	private void Start()
	{
		CreateCube();
	}
	void CreateCube()
	{
		UnityEngine.Profiling.Profiler.BeginSample("Mesh Generation");
		// define vertices
		Vector3[] l_cubeVertices = {

			//front face
			new Vector3(0,0,0),new Vector3(1,0,0),new Vector3(1,1,0), new Vector3(0,1,0),

			//top face

			new Vector3(0,1,0),new Vector3(1,1,0), new Vector3(0,1,1),new Vector3(1,1,1),

			//back face

			new Vector3(0,1,1),new Vector3(1,1,1),new Vector3(0,0,1),new Vector3(1,0,1),
			

			//bot face
			new Vector3(0,0,1),new Vector3(1,0,1),new Vector3(0,0,0),new Vector3(1,0,0),

			//right face
			new Vector3(1,0,0),new Vector3(1,0,1),new Vector3(1,1,0),new Vector3(1,1,1),

			//left face
			new Vector3(0,1,0),new Vector3(0,1,1),new Vector3(0,0,0),new Vector3(0,0,1)
			};


		//define triangles
		/*
		Note: The sequence of the points for the triange should be in clockwise, as the face that contains clockwise direction will be rendered 
		 */

		//24
		int[] l_cubeTriangles =
		 {
			 //front face 
			0,2,1,
			0,3,2,

			 //top face
			4,6,7,
			4,7,5,

			//back face
			9,8,10,
			9,10,11,

			//bottom face
			14,13,12,
			15,13,14,

			//right face
			16,18,19,
			16,19,17,

			//left face
			22,21,20,
			22,23,21

		 };

		//Assign UVS
		Vector2[] l_cubeUVs = new Vector2[]
		{
			//front face
			new Vector2(0.5f,0.5f),
			new Vector2(0.5f,0.25f),
			new Vector2(0.75f,0.25f),
			new Vector2(0.75f,0.5f),
			
			//back face
			new Vector2(0.25f,0.5f),
			new Vector2(0.5f,0.5f),
			new Vector2(0.25f,0.25f),
			new Vector2(0.5f,0.25f),

			//top face
			new Vector2(1f,0.5f),
			new Vector2(0.75f,0.5f),
			new Vector2(1f,0.25f),
			new Vector2(0.75f,0.25f),

			//bot face
			new Vector2(0.5f,0.25f),
			new Vector2(0.75f,0.25f),
			new Vector2(0.5f,0f),
			new Vector2(0.75f,0f),

			//right face
			new Vector2(0.5f,0.5f),
			new Vector2(0.75f,0.5f),
			new Vector2(0.5f,0.75f),
			new Vector2(0.75f,0.75f),

			//left face
			new Vector2(0f,0.5f),
			new Vector2(0.25f,0.5f),
			new Vector2(0f,0.25f),
			new Vector2(0.25f,0.25f),


		};

		// Vector2[] l_cubeUVs = new Vector2[l_cubeVertices.Length];

		// for (int i = 0; i < l_cubeUVs.Length; i++)
		// {
		//     l_cubeUVs[i] = new Vector2(l_cubeVertices[i].x, l_cubeVertices[i].z);
		// }


		Mesh l_mesh = GetComponent<MeshFilter>().mesh;
		l_mesh.Clear();
		l_mesh.vertices = l_cubeVertices;
		l_mesh.triangles = l_cubeTriangles;

		l_mesh.uv = l_cubeUVs;
		l_mesh.RecalculateNormals();

		UnityEngine.Profiling.Profiler.EndSample();

		gameObject.AddComponent<BoxCollider>();
		gameObject.AddComponent<Rigidbody>();
		// Rigidbody l_rg = gameObject.AddComponent<Rigidbody>();
		// l_rg.AddForce(10.0f, );


	}


}
