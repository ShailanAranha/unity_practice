﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformation : MonoBehaviour
{
	[SerializeField]
	MeshFilter m_meshFilter;

	Vector3[] l_vertices;

	int[] l_triangles;

	[SerializeField]
	float xScale, yScale, ZScale;

	private void Start()
	{
		DeformMesh();
	}
	// bool W, A, S, D;
	// private void Update()
	// {

	// 	if (Input.GetKeyDown(KeyCode.W))
	// 	{
	// 		W = true;
	// 	}

	// 	if (Input.GetKeyUp(KeyCode.W))
	// 	{
	// 		W = false;
	// 	}
	// 	if (W)
	// 	{
	// 		ZScale = 0.01f;
	// 	}

	// 	if (Input.GetKeyDown(KeyCode.S))
	// 	{
	// 		S = true;
	// 	}

	// 	if (Input.GetKeyUp(KeyCode.S))
	// 	{
	// 		S = false;
	// 	}

	// 	if (S)
	// 	{
	// 		ZScale = -0.01f;
	// 	}


	// 	if (Input.GetKeyDown(KeyCode.A))
	// 	{
	// 		A = true;
	// 	}

	// 	if (Input.GetKeyUp(KeyCode.A))
	// 	{
	// 		A = false;
	// 	}
	// 	if (A)
	// 	{
	// 		xScale = -0.01f;
	// 	}


	// 	if (Input.GetKeyDown(KeyCode.D))
	// 	{
	// 		D = true;
	// 	}

	// 	if (Input.GetKeyUp(KeyCode.D))
	// 	{
	// 		D = false;
	// 	}
	// 	if (D)
	// 	{
	// 		xScale = 0.01f;
	// 	}

	// 	DeformMesh();
	// 	ZScale = 0f;
	// 	yScale = 0f;
	// 	xScale = 0f;
	// }

	void DeformMesh()
	{
		Mesh l_mesh = m_meshFilter.mesh;
		l_vertices = l_mesh.vertices;
		l_triangles = l_mesh.triangles;

		l_mesh.Clear();

		for (int i = 0; i < l_vertices.Length; i++)
		{
			l_vertices[i] = new Vector3(l_vertices[i].x + xScale, l_vertices[i].y + yScale, l_vertices[i].z + ZScale);
		}
		l_mesh.vertices = l_vertices;
		Array.Reverse(l_triangles);
		l_mesh.triangles = l_triangles;

		l_mesh.RecalculateNormals();
		gameObject.AddComponent<BoxCollider>();
	}


	private void OnDrawGizmos()
	{
		if (l_vertices == null)
		{
			return;
		}

		for (int i_index = 0; i_index < l_vertices.Length; i_index++)
		{
			Gizmos.DrawSphere(l_vertices[i_index], 0.01f);
		}
	}
}



// public static class ExtensionMethod
// {
// 	static int m_intSeed = DateTime.Now.Second;
// 	private static System.Random rng = new System.Random(m_intSeed);
// 	public static void Shuffle(this Array a_list)
// 	{
// 		int n = a_list.Length;
// 		while (n > 1)
// 		{
// 			n--;
// 			int k = rng.Next(n + 1);
// 			var value = a_list[k];
// 			a_list[k] = a_list[n];
// 			a_list[n] = value;
// 		}
// 	}
// }
