﻿
using UnityEngine;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class WaterWaves : MonoBehaviour
{
	Mesh m_mesh;
	Vector3[] m_Vectices;

	[Space]

	[Header("Triangles")]
	int[] m_triangles;

	[SerializeField]
	[Header("Mesh Size")]
	int m_intXsize = 20, m_intZsize = 20;

	[Space]

	[SerializeField]
	[Header("Scale")]
	[Tooltip("Scale Values")]
	float m_fltXScale = 0.0f, m_fltYScale = 0.0f, m_fltZScale = 0.0f;

	[SerializeField]
	Transform m_capDirection = null;

	private void Start()
	{
		m_mesh = GetComponent<MeshFilter>().mesh;
		// CreateMesh();
		// UpdateMesh();
	}

	private void Update()
	{
		CreateMesh();
		UpdateMesh();

		// float l_fltDeltaTime = Time.deltaTime;
		// m_fltXScale += 0.50f * l_fltDeltaTime;
		// m_fltZScale += 0.50f * l_fltDeltaTime;

		float l_fltDeltaTime = Time.deltaTime;
		Scale(m_capDirection.position.x, m_capDirection.position.y, l_fltDeltaTime);
	}

	void Scale(float a_x, float a_z, float a_deltaTime)
	{

		a_x = Mathf.Max(a_x, 20.0f);
		a_z = Mathf.Max(a_z, 20.0f);

		m_fltXScale += (a_x / 20) * a_deltaTime;
		m_fltZScale += (a_z / 20) * a_deltaTime;
	}

	void CreateMesh()
	{
		m_Vectices = new Vector3[(m_intXsize + 1) * (m_intZsize + 1)];

		for (int i = 0, z_index = 0; z_index <= m_intZsize; z_index++)
		{
			for (int x_index = 0; x_index <= m_intXsize; x_index++)
			{
				// float l_y = Mathf.PerlinNoise(x_index * 0.3f + m_fltXScale, z_index * 0.3f + m_fltZScale) * 2f;

				float l_y;
				if (x_index < m_intXsize - 7 && z_index < m_intZsize - 7)
					l_y = Mathf.PerlinNoise(x_index * 0.3f + m_fltXScale, z_index * 0.3f + m_fltZScale); //* 2f;
				else
					l_y = Mathf.PerlinNoise(x_index * 0.3f - m_fltXScale, z_index * 0.3f - m_fltZScale);// * 2f;


				m_Vectices[i] = new Vector3(x_index, l_y, z_index);
				i++;
			}
		}

		m_triangles = new int[m_intXsize * m_intZsize * 6];
		int l_vert = 0, l_tri = 0;

		for (int i = 0; i < m_intZsize; i++)
		{

			for (int x = 0; x < m_intXsize; x++)
			{
				m_triangles[l_tri + 0] = l_vert + 0;
				m_triangles[l_tri + 1] = l_vert + m_intXsize + 1;
				m_triangles[l_tri + 2] = l_vert + 1;
				m_triangles[l_tri + 3] = l_vert + 1;
				m_triangles[l_tri + 4] = l_vert + m_intXsize + 1;
				m_triangles[l_tri + 5] = l_vert + m_intXsize + 2;

				l_vert++;
				l_tri += 6;
			}

			l_vert++;
		}

	}

	void UpdateMesh()
	{
		m_mesh.Clear();

		m_mesh.vertices = m_Vectices;
		m_mesh.triangles = m_triangles;

		m_mesh.RecalculateNormals();
	}

	private void OnDrawGizmos()
	{
		if (m_Vectices == null)
		{
			return;
		}

		for (int i_index = 0; i_index < m_Vectices.Length; i_index++)
		{
			Gizmos.DrawSphere(m_Vectices[i_index], 0.1f);
		}
	}
}
