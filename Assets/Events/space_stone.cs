﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class space_stone : MonoBehaviour {

	public GameObject spawnpoint;


	void OnEnable()
	{
		EventManager.OnClick += drop; 
		EventManager.OnClick += ColorChange;

		//EventManager.toggle += toggleDrop;
	}

	void OnDisable()
	{
		EventManager.OnClick -= drop;
		EventManager.OnClick -= ColorChange;

		//EventManager.toggle -= toggleDrop;
	}

	void drop()
	{
		gameObject.AddComponent<Rigidbody> ().AddForce (transform.forward * 100);


	}
		
	void ColorChange()
	{
		gameObject.GetComponent<Renderer> ().material.color = Color.blue;

	}

//	void toggleDrop()
//	{
//		
//		Destroy(GetComponent<Rigidbody> ());
//		transform.position = spawnpoint.transform.position;
//		transform.rotation = spawnpoint.transform.rotation;
//
//	}
}
