﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mind_stone : MonoBehaviour {

	void OnEnable()
	{
		
		EventManager.OnClick += ChangeColor;
		EventManager.OnClick += backdrop;

	

	}

	void OnDisable()
	{
		EventManager.OnClick -= ChangeColor;
		EventManager.OnClick -= backdrop;
	}



	void ChangeColor()
	{
		gameObject.GetComponent<Renderer> ().material.color = Color.yellow;
	

	}

	void backdrop()
	{
		gameObject.AddComponent<Rigidbody> ().AddForce (-(transform.forward*100));
	}


}
