﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class OneLife : MonoBehaviour
{
	public RectTransform m_rectPosition = null;

	public bool m_boolLife = false;

	[SerializeField]
	public Image m_imgLife = null;

	[SerializeField]
	GameOfLife m_GameOfLifeRef = null;


	public void OnUpdate()
	{
		if (m_boolLife)
		{
			m_imgLife.enabled = false;
		}

		else
		{
			m_imgLife.enabled = true;
		}
	}


	public void OnClickOneLife()
	{
		if (m_GameOfLifeRef.m_boolIsUpdate)
		{
			return;
		}

		m_imgLife.enabled = false;
		m_boolLife = true;
	}
}
