﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOfLife : MonoBehaviour
{

	[SerializeField]
	OneLife m_tempLife = null;

	[SerializeField]
	Transform m_tranGridParent = null;

	// [SerializeField]
	private bool[,] m_boolWorld = new bool[77, 35];

	private OneLife[,] m_World = new OneLife[77, 35];

	private void Start()
	{
		CreateGrid();
		// Debug.LogError(m_boolWorld.Length);
		// Debug.LogError(m_boolWorld.GetLength(1));
		// Debug.LogError(m_boolWorld.GetLength(0));
		// Debug.LogError(m_boolWorld.GetLength(3));


	}


	// void CreateWorld()
	// {
	// 	int l_intDimension1Length = m_World.GetLength(0);
	// 	int l_intDimension2Length = m_boolWorld.GetLength(1);

	// 	for (int i = 0; i < l_intDimension1Length; i++)
	// 	{

	// 		for (int j = 0; j < l_intDimension2Length; j++)
	// 		{
	// 			if (!m_boolWorld[i, j])
	// 			{
	// 				OneLife l_life = Instantiate(m_goDead, m_tranGridParent);
	// 				l_life.m_rectPosition.anchoredPosition = new Vector2(i * 25, j * 25);
	// 			}
	// 		}

	// 	}
	// }

	void CreateGrid()
	{
		int l_intDimension1Length = m_boolWorld.GetLength(0);
		int l_intDimension2Length = m_boolWorld.GetLength(1);

		for (int i = 0; i < l_intDimension1Length; i++)
		{

			for (int j = 0; j < l_intDimension2Length; j++)
			{
				if (!m_boolWorld[i, j])
				{
					OneLife l_life = Instantiate(m_tempLife, m_tranGridParent);
					l_life.m_rectPosition.anchoredPosition = new Vector2(i * 25, j * 25);

					m_World[i, j] = l_life;
				}
			}

		}

		m_tempLife.gameObject.SetActive(false);
	}

	#region Manage Update

	public bool m_boolIsUpdate = false;
	private void Update()
	{
		if (!m_boolIsUpdate)
		{
			return;
		}

		int l_intDimension1Length = m_World.GetLength(0);
		int l_intDimension2Length = m_World.GetLength(1);

		for (int i = 0; i < l_intDimension1Length; i++)
		{

			for (int j = 0; j < l_intDimension2Length; j++)
			{

				// for debug

				/* 
				if (m_World[i, j] == null)
				{
					Debug.LogError("i:" + i + " j:" + j);
					continue;
				}
				*/

				m_World[i, j].OnUpdate();
			}

		}
	}


	public void OnClickStart()
	{
		m_boolIsUpdate = true;
	}

	public void OnClickStop()
	{
		m_boolIsUpdate = false;
	}

	#endregion







}
