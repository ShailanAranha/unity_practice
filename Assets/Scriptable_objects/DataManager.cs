﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEditor;
using UnityEngine;
using System;

public class DataManager : MonoBehaviour
{


	[SerializeField]
	List<DataAttributes> m_lstDataAttributes;

	void Awake()
	{
		ReadFromScriptableObject();
	}


	void ReadFromScriptableObject()
	{
		//  Debug.Log("Loading Assets");
		DataDB l_DataDB = Resources.Load<DataDB>("Scriptable_objects/DataDB");
		m_lstDataAttributes = l_DataDB.lstOfDataAttributes;

		//  Debug.Log("m_lstENAQuestionData is count " + m_lstENAQuestionData.Count);
	}

	[MenuItem("tool/CreateData")]
	public static void CreateScriptableObject()
	{
		ScriptableObject l_outputObj = ScriptableObject.CreateInstance(typeof(DataDB));
		DataDB l_DataDB = (DataDB)l_outputObj;
		string str_path = "Assets/Resources/Scriptable_objects/Data.xml";
		string m_str_asset = File.ReadAllText(str_path);
		ReadFromXML(m_str_asset, l_DataDB);
		AssetDatabase.CreateAsset(l_outputObj, "Assets/Resources/Scriptable_objects/DataList.asset");
		AssetDatabase.SaveAssets();

	}


	public static void ReadFromXML(string strLoaded, DataDB a_DataDB)
	{
		ReadFile(strLoaded, a_DataDB);
	}

	static void ReadFile(string a_stLoaded, DataDB a_DataDB)
	{
		//TextAsset l_textAsset = Resources.Load<TextAsset>(m_filePath); 
		XmlDocument l_document = new XmlDocument();
		l_document.LoadXml(a_stLoaded);
		XmlNode l_node = l_document.SelectSingleNode("/Datas");
		XmlElement l_element = (XmlElement)l_node;
		ParseXMLDocumentSO(ref l_element, a_DataDB);

	}

	static void ParseXMLDocumentSO(ref XmlElement a_element, DataDB a_DataDB)
	{
		XmlNodeList l_xmlKeywordsList = a_element.GetElementsByTagName("Data");

		a_DataDB.lstOfDataAttributes = new List<DataAttributes>();



		foreach (XmlNode l_xmlKeywords in l_xmlKeywordsList)
		{
			DataAttributes m_Data = new DataAttributes();

			XmlElement l_xmlElement = (XmlElement)l_xmlKeywords;

			m_Data.name = l_xmlElement.Attributes["name"].Value;
			m_Data.ID = l_xmlElement.Attributes["ID"].Value;
			m_Data.Age = int.Parse(l_xmlElement.Attributes["Age"].Value);

			a_DataDB.lstOfDataAttributes.Add(m_Data);
		}


	}




}
