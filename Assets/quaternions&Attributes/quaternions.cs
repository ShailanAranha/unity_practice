﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class quaternions : MonoBehaviour {

	public Transform target;

	[SerializeField] [Range(-50,50)]
	protected float speed; 
	void Update()
	{
		Vector3 relativePos = target.position - transform.position;

		// for qutarnion
		transform.rotation = Quaternion.LookRotation (relativePos);
	     
		//for attribute
		//transform.Rotate (new Vector3 (0, speed*Time.deltaTime,0));
	}
}