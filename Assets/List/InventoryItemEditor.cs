﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class InventoryItemEditor : EditorWindow{

	public InventoryList inventorylist;

	[MenuItem("Tool/InventoryItems")]
	static void Init()
	{
		EditorWindow.GetWindow (typeof(InventoryItemEditor));
	}
		
	void OnGUI()
	{
		GUILayout.Label ("Inventory Item Window: ", EditorStyles.boldLabel);
	}
}
