﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryList : ScriptableObject
{
	public List<InventoryItem> itemList;
}
