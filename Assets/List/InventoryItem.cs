﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryItem {

	public string itemName = "New Item";
	public string itemtype = "type";
	public Texture2D itemIcon = null;
	public Rigidbody itemObject = null;
	public bool isUnique = false;
	public bool destroyOnUse = false;
}
