﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName= "new file", menuName= "scriptableObjects")]

public class TestScriptableObjects : ScriptableObject{

	private string name;
	private string id;
	public Sprite img;
}
