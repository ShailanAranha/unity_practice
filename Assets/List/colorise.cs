﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class colorise : EditorWindow{
	Color color;

	[MenuItem("Tool/colorise")]
	static void Init()
	{
		EditorWindow.GetWindow(typeof(colorise));
	}
	void OnGUI()
	{
		Debug.LogError("colorize ongui called");
		GUILayout.Label ("Colorise");
		color = EditorGUILayout.ColorField ("Color", color);
		if (GUILayout.Button ("colorise!!!!!")) {
			OnColorise ();
		
		}

	}

	void OnColorise()
	{
		foreach (GameObject obj in Selection.gameObjects) {
			Renderer renderer = obj.GetComponent<Renderer> ();
			if (renderer != null) {
				renderer.sharedMaterial.color= color;
			}

		}

	}
}
