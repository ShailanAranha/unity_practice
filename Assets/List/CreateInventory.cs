﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateInventory {
	[MenuItem("Assets/Create/Inventory Item List")]
	public static InventoryList Create()
	{
		InventoryList asset = ScriptableObject.CreateInstance<InventoryList> ();

		AssetDatabase.CreateAsset (asset, "Assets/InventoryList.asset");
		AssetDatabase.SaveAssets ();
		return asset;
	}
}
