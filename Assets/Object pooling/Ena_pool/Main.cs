﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{

	List<Answer> m_lstAnsPool = new List<Answer>();

	[SerializeField]
	Answer m_answer = null;

	[SerializeField]
	GameObject m_answerParent = null;

	int m_intPoolAmount = 10;

	void CreatePool()
	{
		// m_goAnswerOption.SetActive(false);
		// m_lstAnswerOptions.Add(m_goAnswerOption);


		for (int i = 0; i < m_intPoolAmount; i++)
		{
			Answer l_ans = Instantiate(m_answer, m_answerParent.transform);

			l_ans.gameObject.SetActive(false);
			m_lstAnsPool.Add(l_ans);
		}

	}

	private void Start()
	{
		CreatePool();
	}

	Answer getpooled()
	{
		for (int i = 0; i < m_lstAnsPool.Count; i++)
		{
			if (!m_lstAnsPool[i].gameObject.activeInHierarchy)
				return m_lstAnsPool[i];
		}

		// if (m_boolWillGrow)
		// {
		// 	GameObject obj = Instantiate(m_lstAnsPool, m_goAnswerOptionParent.transform);
		// 	// GameObject obj = (GameObject)Instantiate(m_goAnswerOption);
		// 	// obj.transform.SetParent(m_goAnswerOptionParent.transform);
		// 	// obj.transform.localPosition = m_goAnswerOption.transform.position;
		// 	// obj.transform.localRotation = m_goAnswerOption.transform.rotation;
		// 	// obj.transform.localScale = m_goAnswerOption.transform.localScale;
		// 	m_lstAnswerOptions.Add(obj);
		// 	return obj;
		// }

		return null;
	}

	/* 
		#region Object Pooling
		void AnswerOptionPool()
		{
			m_goAnswerOption.gameObject.SetActive(false);
			m_lstAnswerOptions.Add(m_goAnswerOption);

			//UnityEngine.Profiling.Profiler.BeginSample("Object Pooling");
			//Debug.LogError("object pooling");
			for (int i = 0; i < m_intPoolAmount; i++)
			{
				UIKnowledgeCkeckAnswer l_obj = Instantiate(m_goAnswerOption, m_goAnswerOptionParent.transform);
				// GameObject obj = (GameObject)Instantiate(m_goAnswerOption);
				// obj.transform.SetParent(m_goAnswerOptionParent.transform);
				// obj.transform.localPosition = m_goAnswerOption.transform.localPosition;
				// obj.transform.localRotation = m_goAnswerOption.transform.localRotation;
				// obj.transform.localScale = m_goAnswerOption.transform.localScale;
				l_obj.gameObject.SetActive(false);
				m_lstAnswerOptions.Add(l_obj);
			}
			//UnityEngine.Profiling.Profiler.EndSample();

		}

		UIKnowledgeCkeckAnswer GetPooledObject()
		{
			for (int i = 0; i < m_lstAnswerOptions.Count; i++)
			{
				if (!m_lstAnswerOptions[i].gameObject.activeInHierarchy)
					return m_lstAnswerOptions[i];
			}

			if (m_boolWillGrow)
			{
				UIKnowledgeCkeckAnswer l_obj = Instantiate(m_goAnswerOption, m_goAnswerOptionParent.transform);
				// GameObject obj = (GameObject)Instantiate(m_goAnswerOption);
				// obj.transform.SetParent(m_goAnswerOptionParent.transform);
				// obj.transform.localPosition = m_goAnswerOption.transform.position;
				// obj.transform.localRotation = m_goAnswerOption.transform.rotation;
				// obj.transform.localScale = m_goAnswerOption.transform.localScale;
				m_lstAnswerOptions.Add(l_obj);
				return l_obj;
			}

			return null;
		}

		#endregion
	*/
}
