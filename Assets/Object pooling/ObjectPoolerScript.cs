﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolerScript : MonoBehaviour {

	public static ObjectPoolerScript current;
	public GameObject PooledObject;
	public int PooledAmount = 10;
	public bool willGrow = true;

	public  List<GameObject> PooledObjects = new List<GameObject>();

	void Awake()
	{
		current = this;
	}

	void Start()
	{
		for (int i = 0; i < PooledAmount; i++) {

			GameObject obj = (GameObject)Instantiate (PooledObject);
			obj.SetActive (false);
			PooledObjects.Add (obj);
		}
	
	}

	public GameObject GetPooled()
	{
		for (int i = 0; i < PooledObjects.Count; i++) {
			if (!PooledObjects [i].activeInHierarchy) {
				return PooledObjects [i];
			}

		}

		if (willGrow) {
			GameObject obj = (GameObject)Instantiate (PooledObject);
			PooledObjects.Add (obj);
			return obj;
		}

		return null;

	}
}
