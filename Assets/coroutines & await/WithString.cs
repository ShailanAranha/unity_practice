﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WithString : MonoBehaviour {

	protected float smoothing =1f;
	public Transform target;


	// Use this for initialization
	void Start () {


		StartCoroutine ("test", target);
		//transform.position = new Vector3 (3f,0,0);
		Debug.Log("working async");

		StopCoroutine ("test");
		Debug.Log("coroutine stopped");
	}



	IEnumerator Test(Transform target){

		while(Vector3.Distance(transform.position, target.position) > 0.01f){

			transform.position = Vector3.Lerp (transform.position, target.position, smoothing * Time.deltaTime);
			yield return null;

		}
		yield return new WaitForSeconds(2);
		Debug.Log ("reached");

	}
}
