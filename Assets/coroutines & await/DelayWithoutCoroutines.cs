﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayWithoutCoroutines : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// DelayWithoutCoroutines.test(null);
		// test();
		// delay();
		// lerp();
		// InvokeRepeating("lerp",0.1f,0.01f);

		TestIsKeyword();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void delay()
	{
		message1();
		Invoke("message2",5);
		
	}

	void message1()
	{
		Debug.Log("message1");
	}

	void message2()
	{
		Debug.Log("message2");

	}

	Vector3 target = new Vector3(1,0,0);
	void lerp()
	{
		transform.position = Vector3.Lerp(transform.position,target,1*Time.deltaTime);

//swapping without temp
		//int a=5,b=8;
		// a = a + b;//5+8=13
		// b=a-b;//13-8=5
		// a=a-b;//13-5=8
	}

	static void test()
	{		
		DelayWithoutCoroutines DelayWithoutCoroutinesRef= new DelayWithoutCoroutines();

		DelayWithoutCoroutinesRef.delay();
	
	}

	// 	static void test(DelayWithoutCoroutines DelayWithoutCoroutines)
	// {
	// 	DelayWithoutCoroutines.delay();	
	// }


	//is keyword

	IEnumerable text="text is a string";

    // IEnumerable text1= 10;

    // float m_fltText = 10.0f;
    void TestIsKeyword()
	{
		
		if(text is string)
		{
			Debug.Log(text);
		}

	}

    
}
