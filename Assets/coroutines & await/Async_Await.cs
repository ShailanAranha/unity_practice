﻿using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

public class Async_Await : MonoBehaviour
{

	int? a = 5;

	int? b;
	int? c = null;
	float? d = null;
	bool? e = null;


	/// <summary>
	/// cannot be serialize
	/// </summary>
	[SerializeField]
	int? m_int;
	private void Start()
	{
		b = a;
		a = 15;

		Debug.Log("b:" + b);
		Debug.Log("a:" + a);


		Delay();
	}

	async void Delay()
	{
		// await new WaitForSeconds(5.0f);
		// Debug.Log("Waiting 3 second...");
		CustomDebug.C_Log("CustomMessage");
		await Task.Delay(TimeSpan.FromSeconds(3));
		Debug.Log("Done!");
	}

}


public static class ExtensionMethod
{
	public static void CustomLog(this Debug a_debug, string a_strMessage)
	{
		Debug.Log(a_strMessage);
	}

}

public class CustomDebug : Debug
{

	public static bool isDebug = false;

	public static void C_Log(string a_strMessage)
	{
		if (isDebug)
			Log(a_strMessage);
	}

	public static void c_LogError(string a_strMessage)
	{
		if (isDebug)
			LogError(a_strMessage);

	}

}
