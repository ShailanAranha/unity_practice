﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TempInputField : InputField
{

	/* 
	protected new void LateUpdate()
	{
		// Only activate if we are not already activated.
		if (m_ShouldActivateNextUpdate)
		{
			if (!isFocused)
			{
				ActivateInputFieldInternal();
				m_ShouldActivateNextUpdate = false;
				return;
			}

			// Reset as we are already activated.
			m_ShouldActivateNextUpdate = false;
		}

		if (InPlaceEditing() || !isFocused)
			return;

		AssignPositioningIfNeeded();

		if (m_Keyboard == null || m_Keyboard.status != TouchScreenKeyboard.Status.Visible)
		{
			if (m_Keyboard != null)
			{
				if (!m_ReadOnly)
					text = m_Keyboard.text;

				if (m_Keyboard.status == TouchScreenKeyboard.Status.Canceled)
					m_WasCanceled = true;
			}

			OnDeselect(null);
			return;
		}

		string val = m_Keyboard.text;

		if (m_Text != val)
		{
			if (m_ReadOnly)
			{
				m_Keyboard.text = m_Text;
			}
			else
			{
				m_Text = "";

				for (int i = 0; i < val.Length; ++i)
				{
					char c = val[i];

					if (c == '\r' || (int)c == 3)
						c = '\n';

					if (onValidateInput != null)
						c = onValidateInput(m_Text, m_Text.Length, c);
					else if (characterValidation != CharacterValidation.None)
						c = Validate(m_Text, m_Text.Length, c);

					if (lineType == LineType.MultiLineSubmit && c == '\n')
					{
						m_Keyboard.text = m_Text;

						OnDeselect(null);
						return;
					}

					if (c != 0)
						m_Text += c;
				}

				if (characterLimit > 0 && m_Text.Length > characterLimit)
					m_Text = m_Text.Substring(0, characterLimit);

				if (m_Keyboard.canGetSelection)
				{
					UpdateCaretFromKeyboard();
				}
				else
				{
					caretPositionInternal = caretSelectPositionInternal = m_Text.Length;
				}

				// Set keyboard text before updating label, as we might have changed it with validation
				// and update label will take the old value from keyboard if we don't change it here
				if (m_Text != val)
					m_Keyboard.text = m_Text;

				SendOnValueChangedAndUpdateLabel();
			}
		}
		else if (m_HideMobileInput && m_Keyboard.canSetSelection)
		{
			m_Keyboard.selection = new RangeInt(caretPositionInternal, caretSelectPositionInternal - caretPositionInternal);
		}
		else if (m_Keyboard.canGetSelection && !m_HideMobileInput)
		{
			UpdateCaretFromKeyboard();
		}


		if (m_Keyboard.status != TouchScreenKeyboard.Status.Visible)
		{
			if (m_Keyboard.status == TouchScreenKeyboard.Status.Canceled)
				m_WasCanceled = true;

			OnDeselect(null);
		}
	}

*/

	private void AssignPositioningIfNeeded()
	{
		// if (m_TextComponent != null && caretRectTrans != null &&
		// 	(caretRectTrans.localPosition != m_TextComponent.rectTransform.localPosition ||
		// 	 caretRectTrans.localRotation != m_TextComponent.rectTransform.localRotation ||
		// 	 caretRectTrans.localScale != m_TextComponent.rectTransform.localScale ||
		// 	 caretRectTrans.anchorMin != m_TextComponent.rectTransform.anchorMin ||
		// 	 caretRectTrans.anchorMax != m_TextComponent.rectTransform.anchorMax ||
		// 	 caretRectTrans.anchoredPosition != m_TextComponent.rectTransform.anchoredPosition ||
		// 	 caretRectTrans.sizeDelta != m_TextComponent.rectTransform.sizeDelta ||
		// 	 caretRectTrans.pivot != m_TextComponent.rectTransform.pivot))
		// {
		// 	caretRectTrans.localPosition = m_TextComponent.rectTransform.localPosition;
		// 	caretRectTrans.localRotation = m_TextComponent.rectTransform.localRotation;
		// 	caretRectTrans.localScale = m_TextComponent.rectTransform.localScale;
		// 	caretRectTrans.anchorMin = m_TextComponent.rectTransform.anchorMin;
		// 	caretRectTrans.anchorMax = m_TextComponent.rectTransform.anchorMax;
		// 	caretRectTrans.anchoredPosition = m_TextComponent.rectTransform.anchoredPosition;
		// 	caretRectTrans.sizeDelta = m_TextComponent.rectTransform.sizeDelta;
		// 	caretRectTrans.pivot = m_TextComponent.rectTransform.pivot;
		// }
	}

	protected override void LateUpdate()
	{

		base.LateUpdate();

		overrideValuesOfAssignPositioningIfNeeded();
	}


	void overrideValuesOfAssignPositioningIfNeeded()
	{

	}
}
