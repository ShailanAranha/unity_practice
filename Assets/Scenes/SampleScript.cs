﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;


public class SampleScript : MonoBehaviour
{
	// Start is called before the first frame update
	void Start()
	{
		// clone();

		int l_Outsum = 0;
		AddByOut(5, 4, out l_Outsum);

		Debug.LogError("Sum:" + l_Outsum);

		AddByOut(5, 10, out l_Outsum);

		Debug.LogError("Sum:" + l_Outsum);


		int l_refSum = 10;
		AddByRef(10, ref l_refSum);

		Debug.LogError("Sum Increment:" + l_refSum);

	}

	// Update is called once per frame
	void Update()
	{

	}


	void AddByOut(int a_NUM1, int a_NUM2, out int Sum)
	{
		Sum = a_NUM1 + a_NUM2;
	}

	void AddByRef(int a_NUM1, ref int Sum)
	{
		Sum += a_NUM1;
	}


	int num = 4;
	public GameObject options;
	public List<GameObject> AnsOptions = new List<GameObject>();

	void clone()
	{

		// for (int i = 0; i < num; i++) {

		// 	GameObject obj = (GameObject)Instantiate (options);
		// 	// obj.SetActive (false);
		// 	AnsOptions.Add (obj);
		// }

		for (int i = 0; i < num; i++)
		{
			var temp = Instantiate(options);
			temp.GetComponentInChildren<Text>().text = i.ToString();

		}

	}

	List<int> finalanswer = new List<int>();
	List<string> submitcorrect = new List<string>();
	List<string> submitwrong = new List<string>();
	List<string> submitoutput = new List<string>();
	void feedback()
	{
		if (finalanswer.Count != submitcorrect.Count)
		{
			//Some are right some are wrong
			if (finalanswer.Count < submitoutput.Count)
			{

				if (submitwrong.Count == 0)
				{
					//some right are remaining with no wroong
				}
				else
				{
					//some are right are remaining and some are worong
				}
			}
			else if (finalanswer.Count > submitoutput.Count)
			{
				if (submitcorrect.Count == submitoutput.Count)
				{
					//all correct with some wrong 
				}
				else if (submitcorrect.Count == 0)
				{
					//all wrong
				}
				else
				{
					// some right are remaining with wrong

				}
			}

		}
		else
		{
			//all correct
		}

	}


}
