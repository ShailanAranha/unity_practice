﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DigitalClock : MonoBehaviour
{
	[SerializeField]
	Text m_txtClock = null;

	[SerializeField]
	float m_fltHours, m_fltMinutes, m_fltSecs;

	DateTime m_dntCurrentTime;


	/* Method 1
	private void Start()
	{
		m_dntCurrentTime = DateTime.Now;
		m_fltHours = m_dntCurrentTime.Hour;
		m_fltMinutes = m_dntCurrentTime.Minute;
		m_fltSecs = m_dntCurrentTime.Second;
		// Debug.LogError(m_dntCurrentTime);
	}

	// Update is called once per frame
	void Update()
	{
	 
		if (m_fltSecs >= 60.0f)
		{
			m_fltSecs = 0.0f;
			m_fltMinutes += 1.0f;
		}

		if (m_fltMinutes >= 60.0f)
		{
			m_fltMinutes = 0.0f;
			m_fltHours += 1.0f;
		}

		if (m_fltHours >= 24.0f)
		{
			m_fltHours = 0.0f;
		}

		m_fltSecs += Time.deltaTime;


		// m_txtClock.text = m_fltHours.ToString("00") + ":" + m_fltMinutes.ToString("00") + ":" + m_fltSecs.ToString("0.00");
		m_txtClock.text = string.Format("{0:00}:{1:00}:{2:00.00}", m_fltHours, m_fltMinutes, m_fltSecs);

		// for (int i = 0; i < 100; i++)
		// {
		// 	l_temp += "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq";
		// }
     

		DoIt();
		m_txtClock.text = string.Format("{0:00}:{1:00}:{2:00.00}", m_fltHours, m_fltMinutes, m_fltSecs);

	}
   */

	public float m_fltTemp;

	private void Start()
	{
		m_dntCurrentTime = DateTime.Now;
		// m_fltHours = m_dntCurrentTime.Hour;
		// m_fltMinutes = m_dntCurrentTime.Minute;
		Debug.LogError("float.MaxValue:" + float.MaxValue);
		m_fltSecs = m_dntCurrentTime.Second + (m_dntCurrentTime.Minute * 60) + (m_dntCurrentTime.Hour * 60 * 60);
		// Debug.LogError(m_dntCurrentTime);

        Debug.LogError(Convert.ToInt32('h'));
        Debug.LogError(Convert.ToInt32('a'));
        Debug.LogError(Convert.ToInt32('p'));
        Debug.LogError(Convert.ToInt32('r'));
        Debug.LogError(Convert.ToInt32('R'));

	}

	private void Update()
	{
		DoIt();
		m_txtClock.text = string.Format("{0:00}:{1:00}:{2:00.00}", m_fltHours, m_fltMinutes, m_fltSecs % 60);

	}


	void DoIt()
	{
		m_fltSecs = m_fltSecs + Time.deltaTime;
		m_fltMinutes = (int)((m_fltSecs / 60) % 60);
		m_fltTemp = (int)(m_fltSecs / 60);
		m_fltHours = (int)((m_fltSecs / 3600) % 24);

	}
    
}