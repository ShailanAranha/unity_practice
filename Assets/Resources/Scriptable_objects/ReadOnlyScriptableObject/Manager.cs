﻿using System.Collections;
using System.Collections.Generic;
using ReadOnlySO;
using UnityEngine;

public class Manager : MonoBehaviour
{
	MyReadOnlyScriptableObject m_MyReadOnlyScriptableObject = null;
	// Use this for initialization
	void Start()
	{
		m_MyReadOnlyScriptableObject = Resources.Load<MyReadOnlyScriptableObject>("Scriptable_objects/ReadOnlyScriptableObject/ReadOnlyScriptableObject");
		m_MyReadOnlyScriptableObject.addRandoneData();
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
			Resources.UnloadAsset(m_MyReadOnlyScriptableObject);
	}
}
