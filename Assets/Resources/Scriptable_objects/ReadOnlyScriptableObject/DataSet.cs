﻿
using UnityEngine;

namespace ReadOnlySO
{
	[System.Serializable]
	internal class DataSet
	{
		[SerializeField]
		private string m_strID = null;

		internal string Id { get { return m_strID; } }

		[SerializeField]
		private string m_strName = null;

		internal string Name { get { return m_strName; } }
		internal DataSet(string a_strId, string a_strName)
		{
			m_strID = a_strId;
			m_strName = a_strName;
		}

	}

}