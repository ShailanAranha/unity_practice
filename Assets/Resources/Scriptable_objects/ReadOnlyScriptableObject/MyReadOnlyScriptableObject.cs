﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ReadOnlySO
{
	//factory method
	internal class MyReadOnlyScriptableObject : ScriptableObject
	{
		[SerializeField]
		private DataSet[] m_arrDataSet = null;

		internal DataSet[] DataSet { get { return m_arrDataSet; } }

		private void Init(DataSet[] a_arrDataSet)
		{
			m_arrDataSet = a_arrDataSet;
		}

		internal static MyReadOnlyScriptableObject CreateInstance(DataSet[] a_arrDataSet)
		{
			var data = ScriptableObject.CreateInstance<MyReadOnlyScriptableObject>();
			// MyReadOnlyScriptableObject l_objDb = (MyReadOnlyScriptableObject)data;

			data.Init(a_arrDataSet);
			return data;
		}



		/// <summary>
		/// remove this
		/// </summary>
		[SerializeField]
		List<DataSet> l_newdatacheck = null;
		internal void addRandoneData()
		{
			l_newdatacheck = new List<DataSet>();
			DataSet l_data = new DataSet("new", "new");
			l_newdatacheck.Add(l_data);
			
		}
	}
}