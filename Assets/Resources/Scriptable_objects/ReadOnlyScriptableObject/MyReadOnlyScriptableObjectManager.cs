﻿
using UnityEditor;
using UnityEngine;

namespace ReadOnlySO
{
	internal class MyReadOnlyScriptableObjectManager : MonoBehaviour
	{
		[MenuItem("tool/CreateReadOnlyScriptableOBject")]
		internal static void CreateScriptableObject()
		{
			DataSet[] l_arr = new DataSet[] { new DataSet("1", "a"), new DataSet("2", "b"), new DataSet("3", "c") };

			MyReadOnlyScriptableObject l_instance = MyReadOnlyScriptableObject.CreateInstance(l_arr);
			// please dont apply application.datapath
			AssetDatabase.CreateAsset(l_instance, "Assets/Resources/Scriptable_objects/ReadOnlyScriptableObject/ReadOnlyScriptableObject.asset");
			AssetDatabase.SaveAssets();
		}
	}
}
