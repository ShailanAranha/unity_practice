﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadOnlyAndConstants : MonoBehaviour
{
	#region Readonly
	private static readonly string m_rostrTest1 = "string 1";

	//does not require a start value
	private static readonly string m_rostrTest;


	MyReadonlyClass m_objMyReadonlyClass = new MyReadonlyClass(10, "Messi");

	void ReadOnlyMethod()
	{
		//cannot be assigned
		// m_objMyReadonlyClass.ID = 100;


	}

	#endregion


	#region Constant
	private const string m_conststrTest1 = "string 1";

	//require a start value
	// private const string m_contstrTest;

	// cannot be used as static modifier
	// private static const string m_conststrTest2 = "string 2";


	#endregion

}


sealed class MyReadonlyClass
{
	public readonly int ID;

	public readonly string name;

	public MyReadonlyClass(int a_id, string a_name)
	{
		this.ID = a_id;
		this.name = a_name;
	}

}

