﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructors : MonoBehaviour
{

	FCB m_objFCB= new FCB(10, "Messi");
	private void Awake()
	{
		// m_objFCB = new FCB(10, "Messi");

		string test = string.Format("ID:{0}, Name:{1}", m_objFCB.ID, m_objFCB.Name);
		Debug.LogError(test);
	}

}


sealed class FCB
{
	public string Name;
	public int ID;

	public FCB(int a_intid, string a_strNAme)
	{
		this.ID = a_intid;
		this.Name = a_strNAme;
	}

	~FCB()
	{
		Debug.LogError("FCB Destroyed");
	}

}
