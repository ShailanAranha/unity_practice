﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[MyTestAttribute("Attribute name")]
public class AttributeTest : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{
		MyMethod();
	}

	// Update is called once per frame
	void Update()
	{

	}


	[MyTestAttribute("Attribute name")]
	protected virtual void MyMethod()
	{
		MyTestAttribute MyNewAttribute = typeof(AttributeTest).GetMethod("MyMethod").GetCustomAttributes(true).OfType<MyTestAttribute>().FirstOrDefault();
		if (MyNewAttribute == null)
			Debug.LogError("MyNewAttribute cannot bve found");
		else
			Debug.LogError(MyNewAttribute.Name);

		// Get instance of the attribute.
		GetAttribute(typeof(AttributeTest));
	}

	void GetAttribute(Type t)
	{

		MyTestAttribute MyAttribute = (MyTestAttribute)Attribute.GetCustomAttribute(t, typeof(MyTestAttribute));

		if (MyAttribute == null)
			Debug.LogError("Attribute cannot be found");
		else
			Debug.LogError(MyAttribute.Name);

	}
}

[AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
public class MyTestAttribute : Attribute
{
	private string name;
	public MyTestAttribute(string name)
	{

		this.name = name;
	}

	public virtual string Name
	{
		get { return name; }
	}

}

[AttributeUsage(AttributeTargets.Method, Inherited = true)]
public class MyAnotherTestAttribute : Attribute
{

}
