﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StaticCheck : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{
		OriginalAvengers();
		InfinityStones();
		Display();
		Debug.LogError("m_lstInfinityStone:" + m_lstInfinityStone.Count);
	}

	private void Awake()
	{
	}



	public static List<string> m_lstInfinityStone = new List<string>(50);

	int NoOfAvengers = 0;


	void OriginalAvengers()
	{
		NoOfAvengers += 6;
		Debug.LogError("NoOfAvengers:" + NoOfAvengers);
	}

	void InfinityStones()
	{
		UnityEngine.Profiling.Profiler.BeginSample("List Add");
		m_lstInfinityStone.Add("space stone");
		m_lstInfinityStone.Add("power stone");
		m_lstInfinityStone.Add("reality stone");
		m_lstInfinityStone.Add("soul stone");
		m_lstInfinityStone.Add("time stone");
		m_lstInfinityStone.Add("mind stone");

		UnityEngine.Profiling.Profiler.EndSample();

	}


	void Display()
	{
		StringBuilder l_strBd = new StringBuilder();
		IEnumerable<string> disps = from InfinityStone in m_lstInfinityStone select InfinityStone;

		foreach (string disp in disps)
		{
			l_strBd.Append(disp).Append(", ");
		}
		Debug.LogError("Infinity Stones:" + l_strBd);
	}


	private void OnDestroy()
	{

	}


	void ReloadScene()
	{
		SceneManager.LoadScene("null_check");
	}



	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.R))
		{
			ReloadScene();
		}
	}
}
