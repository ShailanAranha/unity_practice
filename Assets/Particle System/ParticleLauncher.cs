﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLauncher : MonoBehaviour {

	[SerializeField]
	protected ParticleSystem particalLauncher;

	[SerializeField]
	protected ParticleSystem splatterParticles;

	public Gradient particalColorGradient;
	// Use this for initialization

	public ParticleDecalPool splatDecalPool;

	List<ParticleCollisionEvent> collisionEvents; 

	void Start()
	{ 
		//splatDecalPool = new ParticleDecalPool ();
		collisionEvents = new List<ParticleCollisionEvent> ();
	}

	void OnParticleCollision(GameObject Other)
	{
		ParticlePhysicsExtensions.GetCollisionEvents (particalLauncher, Other, collisionEvents);
		for (int i = 0; i < collisionEvents.Count; i++) {
			splatDecalPool.HitParticle(collisionEvents[i], particalColorGradient);
			emmision (collisionEvents[i]);

		}

	}

	void emmision(ParticleCollisionEvent particleCollisionEvent)
	{ 
		splatterParticles.transform.position = particleCollisionEvent.intersection;
		splatterParticles.transform.rotation = Quaternion.LookRotation (particleCollisionEvent.normal);
		ParticleSystem.MainModule psmain = splatterParticles.main;
		psmain.startColor = particalColorGradient.Evaluate (Random.Range (0f, 1f));

		splatterParticles.Emit (1);
	}



	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Fire1")) {
			ParticleSystem.MainModule psmain = particalLauncher.main;
			psmain.startColor = particalColorGradient.Evaluate (Random.Range (0f, 1f));
			particalLauncher.Emit (1);

		}

	}
}
