﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class particleDecalData{

	public Vector3 position;
	public Vector3 rotation;
	public float size;
	public Color color;
}
