﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionScript : MonoBehaviour
{

	[SerializeField]
	protected float speed;

	private float h;
	private float v;

	private float horizontal;
	private float vertical;
	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		h = Input.GetAxis("Mouse X") * speed;
		v = Input.GetAxis("Mouse Y") * speed;

		horizontal = Input.GetAxis("Horizontal") * speed;
		vertical = Input.GetAxis("Vertical") * speed;

		transform.Translate(horizontal, 0, vertical);
		// transform.Rotate(h, v, 0);
	}
}
