﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class OnDragScript : MonoBehaviour, IDragHandler
{
	[SerializeField]
	Camera m_UICam = null;
	void IDragHandler.OnDrag(PointerEventData eventData)
	{

		Vector3 screenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
		screenPoint.z = 100.0f;
		transform.position = m_UICam.ScreenToWorldPoint(screenPoint);
	}
}
