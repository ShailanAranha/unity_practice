﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendering : MonoBehaviour
{

	[SerializeField]
	Transform m_trans1, m_trans2, m_trans3;


	Vector3 m_vec1, m_vec2, m_vec3;

	[SerializeField]
	LineRenderer m_line = null;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		DrawLine();
	}


	void DrawLine()
	{
		m_vec1 = new Vector3(m_trans1.transform.position.x, m_trans1.transform.position.y, m_trans1.transform.position.z);
		m_vec2 = new Vector3(m_trans2.transform.position.x, m_trans2.transform.position.y, m_trans2.transform.position.z);
		m_vec3 = new Vector3(m_trans3.transform.position.x, m_trans3.transform.position.y, m_trans3.transform.position.z);

		m_line.positionCount = 3;
		m_line.sortingOrder = 5;
		m_line.SetPosition(0, m_vec1);
		m_line.SetPosition(1, m_vec2);
		m_line.SetPosition(2, m_vec3);
	}

	/* 
		//clear last frame of render texture
				RenderTexture l_rt = UnityEngine.RenderTexture.active;
				UnityEngine.RenderTexture.active = m_RenderTextureRef;
				GL.Clear(true, true, Color.clear);
				UnityEngine.RenderTexture.active = l_rt;
	*/
}
