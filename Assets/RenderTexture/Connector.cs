﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Connector : MonoBehaviour
{

	[SerializeField]
	RectTransform m_rect1, m_rect2, m_rect3, m_rect4;


	Vector2 m_vec1, m_vec2, m_vec3, m_vec4;

	[SerializeField]
	LineRenderer m_line = null;


	// Update is called once per frame
	void Update()
	{
		DrawLine();
	}


	void DrawLine()
	{
		m_vec1 = new Vector2(m_rect1.transform.position.x, m_rect1.transform.position.y);
		m_vec2 = new Vector2(m_rect2.transform.position.x, m_rect2.transform.position.y);
		
		m_line.positionCount = 2;
		// m_line.sortingOrder = 4;
		m_line.SetPosition(0, m_vec1);
		m_line.SetPosition(1, m_vec2);
		
	}

	
}
