﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ui_test : MonoBehaviour
{


	[SerializeField]
	GameObject m_goParent = null;

	void TestComponents()
	{
		Text[] txts = m_goParent.GetComponentsInChildren<Text>();
		// IEnumerator myenum = obj.GetEnumerator();

		// // if(myenum==null)
		// myenum.MoveNext();
		// Debug.LogError(myenum.Current);

		//note: Always use break keyword in a loop if only one element in the collection or array is to be found.

		foreach (Text txt in txts)
		{
			if (txt.name == "strange")
			{
				txt.GetComponent<Text>().text = "we are in the end game now!";
				// break;
			}

			if (txt.name == "stark")
			{
				txt.GetComponent<Text>().text = "that up there, that's... That's the endgame. How were you guys planning on beating that?";
			}
			Debug.LogError("call");
		}
		int a = 5, b = 6;
		bool check = a != b;
		Debug.LogError(check);
	}


	private void Start()
	{
		TestComponents();
	}
}
