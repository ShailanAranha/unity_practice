﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

public class TrackerDropdown : progress_tracker {

	Animator m_anim_TrackerDropdown;


   [SerializeField]	
	protected GameObject m_go_StartLetter;

	 [SerializeField]	
	protected GameObject m_go_ABCDEFGHLabel;

	 [SerializeField]	
	protected GameObject m_go_TrackerSurveyText;

	// get from default vaules
	//  [SerializeField]	
	// protected GameObject m_go_Content;

	[SerializeField]
	List<SliderData> m_lst_DropdownDatas = new List<SliderData>();

	//progress_tracker m_obj_CheckStageValue;

	private Text m_go_StartLetterValue;
	private Text m_go_ABCDEFGHLabelValue;
	private Text m_go_TrackerSurveyTextValue;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{		
     m_anim_TrackerDropdown = GetComponent<Animator>();		
	 m_go_StartLetterValue = m_go_StartLetter.GetComponent<Text>();
	 m_go_ABCDEFGHLabelValue= m_go_ABCDEFGHLabel.GetComponent<Text>();
	 m_go_TrackerSurveyTextValue = m_go_TrackerSurveyText.GetComponent<Text>();
	//m_obj_CheckStageValue = new progress_tracker();
	 
	}
    


	public void displayTrackerContents(string stngID)
    { 	
		// query to find the content 
        var l_lq_DropdownContents = from l_DropdownData in m_lst_DropdownDatas where l_DropdownData.StringID == stngID select l_DropdownData;
		
		

       
		foreach (var l_lq_DropdownContent in l_lq_DropdownContents)
		{
			// tracker dropdown animation method 
		    showTrackerDropdown();
			
			
		// for task inprogress, complete and incomplete
	    //for stage inprogress
			if(l_lq_DropdownContent.StageValue == ProgressStage(false))
			{
			//	Debug.Log("if called"+l_lq_DropdownContent.StageValue+" progress stage value"+ ProgressStage(false));

				//display progress highlighter ui
				l_lq_DropdownContent.Progress_Highlight.SetActive(true);

				//display green color
				m_go_StartLetterValue.color = new Color(250.0f, 196.0f, 0.0f);
				m_go_ABCDEFGHLabelValue.color =new Color(250.0f, 196.0f, 0.0f);
				

				//contents data
				m_go_StartLetterValue.text = l_lq_DropdownContent.Initial.ToString();
				m_go_ABCDEFGHLabelValue.text = l_lq_DropdownContent.ABCDEFGH_label.ToString();
				m_go_TrackerSurveyTextValue.text = l_lq_DropdownContent.tracker_survey_Text.ToString();
			}


		// for stage completed	
			else if(l_lq_DropdownContent.StageValue < ProgressStage(false))
			{
		//Debug.Log("if else called"+l_lq_DropdownContent.StageValue+" progress stage value"+ ProgressStage(false));

			//display progress highlighter ui
			l_lq_DropdownContent.Progress_Highlight.SetActive(true);


			//display green color
			m_go_StartLetterValue.color = Color.green;
			m_go_ABCDEFGHLabelValue.color = Color.green;
			//m_go_TrackerSurveyTextValue.color = new Color(250.0f, 196.0f, 0.0f);

			//contents data
			m_go_StartLetterValue.text = l_lq_DropdownContent.Initial.ToString();
			m_go_ABCDEFGHLabelValue.text = l_lq_DropdownContent.ABCDEFGH_label.ToString();
			m_go_TrackerSurveyTextValue.text = l_lq_DropdownContent.tracker_survey_Text.ToString();
			}


		// for stage incomplete
           else
		   {			   	
			//display green color
			m_go_StartLetterValue.color = Color.grey;
			m_go_ABCDEFGHLabelValue.color = Color.grey;
			   
			//contents data
			m_go_StartLetterValue.text = l_lq_DropdownContent.Initial.ToString();
			m_go_ABCDEFGHLabelValue.text = l_lq_DropdownContent.ABCDEFGH_label.ToString();
			m_go_TrackerSurveyTextValue.text = l_lq_DropdownContent.tracker_survey_Text.ToString();

		   }
			//hide progress highlighter ui
			//l_lq_DropdownContent.Progress_Highlight.SetActive(false);
		}

    }

  

    public void showTrackerDropdown()
	{			
     //on mouse hover enter
	//gameObject.SetActive (true);
  	
	//drop down animation
  // m_anim_TrackerDropdown = GetComponent<Animator>();
	m_anim_TrackerDropdown.SetFloat("tracker_dropdown",0.5f);	
	
	}


	public void hideTrackerDropdown()
	{
    //drop down animation
	m_anim_TrackerDropdown.SetFloat("tracker_dropdown",-0.5f);	


	

	//on mouse hover exit
	//gameObject.SetActive (false);

	}
}
