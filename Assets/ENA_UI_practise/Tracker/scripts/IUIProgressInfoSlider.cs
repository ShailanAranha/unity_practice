﻿using System;
public interface IUIProgressInfoSlider
	{
		void Show();
		void Hide();
		void SlideUp();
		void SlideDown();
void LoadData(StageStatus a_enumssStage, string a_strTitle, string a_strSurveyType, string a_strContent,string a_strInitial);
	}

