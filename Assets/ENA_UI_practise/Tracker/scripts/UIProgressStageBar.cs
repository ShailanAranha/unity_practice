using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;



class UIProgressStageBar:MonoBehaviour
{

    //TrackerBaseButtons
    	[SerializeField]
	List<TrackerBaseButtons_list> m_lst_TrackerBaseButtonsDatas = new List<TrackerBaseButtons_list>();

    [SerializeField]
	float m_flt_ProgressValue = float.MinValue;

	[SerializeField]
	Image m_img_ProgressImage = null;
	//TrackerBaseButtons m_updateButtonColor = null;
 	 

	public float ProgressStage(bool check = true)
	{
		
		if(check)
		{
		m_flt_ProgressValue++;
		}

		return m_flt_ProgressValue;
	}
  
	public void buttonColor()
	{
		var l_lq_TrackerBaseButtonscontents = from m_lst_TrackerBaseButtonsData in m_lst_TrackerBaseButtonsDatas
											  select m_lst_TrackerBaseButtonsData;

		foreach (var l_lq_TrackerBaseButtonscontent in l_lq_TrackerBaseButtonscontents)
		{
//			Debug.Log("check");
			if (l_lq_TrackerBaseButtonscontent.StageValue == ProgressStage(false))
			{
				l_lq_TrackerBaseButtonscontent.TrackerBase_Buttons.GetComponentInChildren<Text>().color = Color.yellow;
			}
			else if (l_lq_TrackerBaseButtonscontent.StageValue < ProgressStage(false))
			{
				l_lq_TrackerBaseButtonscontent.TrackerBase_Buttons.GetComponentInChildren<Text>().color = Color.white;
			}


		}
	}
	public void progress()
	{
		
        m_img_ProgressImage.fillAmount = (1/15f)* ProgressStage();
		buttonColor();

	}

    
}