﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SliderData {

  public string StringID;
  public float StageValue;
  public string tracker_survey_Text;
  public string Initial;
  public string ABCDEFGH_label;
  public string content;
  public GameObject Progress_Highlight;

}
