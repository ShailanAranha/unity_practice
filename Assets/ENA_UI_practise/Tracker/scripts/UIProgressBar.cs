﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIProgressBar : MonoBehaviour {


[SerializeField]
	UIProgressInfoSlider m_UIProgressInfoSliderRef = null;


[SerializeField]
	UIProgressStageBar m_UIProgressStageBarRef = null;

[SerializeField]
	ProgressBarBackground ProgressBarBackgroundRef = null;
	  public void Update()
	  {
	     if (Input.GetKeyDown(KeyCode.Space))
           SetProgress();
        
	  }
	void showTrackerBase()
	{
      gameObject.SetActive(true);
	 
	}


	void hideTrackerBase()
	{
      gameObject.SetActive(false);
	  
	}
     
	void SetProgress()
	{
		m_UIProgressStageBarRef.progress();
	}

	void EnableProgressBarBackground()
	{
      ProgressBarBackgroundRef.enabled = true;		
	} 
	void DisableProgressBarBackground()
	{
      ProgressBarBackgroundRef.enabled = false;		
	} 

	
	
	
}
public enum StageStatus
{
	InComplete ,
	InProgress,
	Complete
}
