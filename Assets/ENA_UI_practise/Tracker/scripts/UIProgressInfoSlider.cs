﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;


public class UIProgressInfoSlider:MonoBehaviour
{
	[SerializeField]
	Animator m_AnimatorRef = null;

	[SerializeField]
	float m_fParaSlideUp= float.MinValue, m_fParSlideDown = float.MinValue;

	[SerializeField]
	string m_strAnimParaMultiplier = string.Empty;

	[SerializeField]
	UIProgressStageBar UIProgressStageBarRef = null;


	[SerializeField]
	Text m_txtTitle=null, m_txtSurveyType=null, m_txtContent=null,m_txtInitial=null;

	[SerializeField]
	Color m_colTitleComplete=Color.clear, m_colTitleInComplete=Color.clear, m_colTitleInProgress=Color.clear;

	[SerializeField]
	Color m_colContentComplete = Color.clear, m_colContentInComplete = Color.clear, m_colContentInProgress = Color.clear;

	[SerializeField]
	Color m_colSurveyComplete=Color.clear, m_colSurveyInComplete=Color.clear, m_colSurveyInProgress=Color.clear;
 void Show() { 
	   
	}

 void Hide() { 
	
	}

 public void SlideUp() {
		m_AnimatorRef.SetFloat(m_strAnimParaMultiplier,m_fParaSlideUp);
	}

 public void SlideDown() { 
		m_AnimatorRef.SetFloat(m_strAnimParaMultiplier,m_fParSlideDown);
	}


 void LoadData(StageStatus a_enumssStage,string a_strTitle,string a_strSurveyType,string a_strContent,string a_strInitial){
		m_txtSurveyType.text = a_strSurveyType;
		m_txtInitial.text = a_strInitial;
		m_txtTitle.text = a_strTitle;
		m_txtContent.text = a_strContent;
		//Debug.LogError(a_enumssStage);
		switch (a_enumssStage){

			case StageStatus.Complete:
				m_txtInitial.color = m_colTitleComplete;
				m_txtTitle.color = m_colTitleComplete;
				m_txtContent.color = m_colContentComplete;
				m_txtSurveyType.color = m_colSurveyComplete;
				break;
			
			case StageStatus.InComplete:
				m_txtInitial.color = m_colTitleInComplete;
				m_txtTitle.color = m_colTitleInComplete;
				m_txtContent.color = m_colContentInComplete;
				m_txtSurveyType.color = m_colSurveyInComplete;

				break;
			
			case StageStatus.InProgress:

				m_txtInitial.color = m_colTitleInProgress;
				m_txtTitle.color = m_colTitleInProgress;
				m_txtContent.color = m_colContentInProgress;
				m_txtSurveyType.color = m_colSurveyInProgress;

				break;

		
		}
	}

	
	StageStatus Status(string stngID)
	{
		 var l_lq_DropdownContents = from l_DropdownData in m_lst_DropdownDatas where l_DropdownData.StringID == stngID select l_DropdownData;
		 foreach (var l_lq_DropdownContent in l_lq_DropdownContents)
		{
			
			if(l_lq_DropdownContent.StageValue == UIProgressStageBarRef.ProgressStage(false))
			{
				return StageStatus.InProgress;
			}
			else if (l_lq_DropdownContent.StageValue < UIProgressStageBarRef.ProgressStage(false))
			{
				return StageStatus.Complete;
			}
			
		}
		return StageStatus.InComplete; ;
	}
    
	[SerializeField]
	List<SliderData> m_lst_DropdownDatas = new List<SliderData>();


	public void displayTrackerContents(string stngID="avpu_id")
    { 	
		// query to find the content 
        IEnumerable<SliderData> l_lq_DropdownContents = from l_DropdownData in m_lst_DropdownDatas where l_DropdownData.StringID == stngID select l_DropdownData;
		
		//SlideDown();
       
		foreach (SliderData l_lq_DropdownContent in l_lq_DropdownContents)
		{
			
			m_txtInitial.text = l_lq_DropdownContent.Initial;
			m_txtTitle.text = l_lq_DropdownContent.ABCDEFGH_label;
			m_txtSurveyType.text = l_lq_DropdownContent.tracker_survey_Text;
			m_txtContent.text = l_lq_DropdownContent.content;
			LoadData(Status(stngID),m_txtTitle.text,m_txtSurveyType.text,m_txtContent.text, m_txtInitial.text);

				if(Status(stngID)==StageStatus.Complete)
	 		l_lq_DropdownContent.Progress_Highlight.SetActive(true);

			 if(Status(stngID)==StageStatus.InProgress)
	 		l_lq_DropdownContent.Progress_Highlight.SetActive(true);
		}

		
    }

	public void HideDropdown()
	{
 	//	 SlideUp();
		// RectTransformUtility.RectangleContainsScreenPoint(Re,Input.mousePosition.);

	}



}

