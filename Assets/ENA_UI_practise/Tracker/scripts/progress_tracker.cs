﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class progress_tracker : MonoBehaviour {

	protected float m_flt_ProgressValue = 0.0f;
	
 	 
	  public virtual void Update()
	  {
	     if (Input.GetKeyDown(KeyCode.Space))
        {
           progress();
        }
	  }

	public float ProgressStage(bool check = true)
	{
		
		if(check)
		{
		m_flt_ProgressValue++;
		}

		return m_flt_ProgressValue;
	}
  
	
	
	public void progress()
	{
		
        gameObject.GetComponent<Image>().fillAmount = (1/15f)* ProgressStage();
		//m_updateButtonColor.buttonColor();

	}



}
