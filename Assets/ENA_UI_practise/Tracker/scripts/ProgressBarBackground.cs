﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBarBackground : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		ShowDropdown();
		
	}

	[SerializeField]
	RectTransform m_rect_TrackerBase;

	[SerializeField]
	UIProgressInfoSlider UIProgressInfoSliderRef = null;
		void ShowDropdown()
	{
		if(RectTransformUtility.RectangleContainsScreenPoint(m_rect_TrackerBase,Input.mousePosition))
				UIProgressInfoSliderRef.SlideDown();
				else
				UIProgressInfoSliderRef.SlideUp();
						
	}
}
