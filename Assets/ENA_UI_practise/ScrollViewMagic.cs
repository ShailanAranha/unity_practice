﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewMagic : MonoBehaviour
{

	[SerializeField]
	ScrollRect m_scroll = null;

	[SerializeField]
	Image m_imgUIMask = null, m_bgImage = null;

	[SerializeField]
	GameObject m_goScrollVertical = null;

	[SerializeField]
	Image m_imgContent = null;

	[SerializeField]
	RectTransform m_rectContent = null;


	void SpellToActivateScrollView()
	{


		if (m_rectContent.rect.height >= 80)
		{
			m_scroll.enabled = true;
			m_imgUIMask.enabled = true;
			m_goScrollVertical.SetActive(true);
		}


	}


	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		SpellToActivateScrollView();
		Debug.LogError(m_rectContent.rect.height);
	}
}
