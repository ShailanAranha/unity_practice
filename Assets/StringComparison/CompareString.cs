﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompareString : MonoBehaviour
{

	string string1 = "this is string1";

	string string2 = "this is strin56";

	static char[] char1 = { 'a', 'b', 'c' };

	string string3 = new string(char1);

	string string4 = "abc";

	string string5;

	string string6;
	int a;


	void ReferenceCheck()
	{
		string s1 = "";
		string s2 = "";
		CompareString.Equals(s1, s2);
		s1.Equals(s2);
		string.Equals(s1, s2);

		string5 = "we are in the end game now!";
		string6 = string5;
		string6 = "that up there, that's... That's the endgame. How were you guys planning on beating that?";
		Debug.LogError("string5:" + string5 + " string6:" + string6);
		Debug.LogError("reference equality:" + object.ReferenceEquals(string5, string6));

	}


	private void Start()
	{


		UnityEngine.Profiling.Profiler.BeginSample("Using operator");
		for (int i = 0; i < 100; i++)
		{
			Debug.LogError("string1 and string2 is equal:" + (string1 == string2));
		}
		UnityEngine.Profiling.Profiler.EndSample();

		UnityEngine.Profiling.Profiler.BeginSample("Using equals");
		for (int i = 0; i < 100; i++)
		{
		Debug.LogError("string1 and string2 is equal:" + string3.Equals(string4));

		}
		UnityEngine.Profiling.Profiler.EndSample();

		UnityEngine.Profiling.Profiler.BeginSample("Using equals ordinal");
		for (int i = 0; i < 100; i++)
		{
		Debug.LogError("string1 and string2 is equal:" + string3.Equals(string4, StringComparison.Ordinal));

		}
		UnityEngine.Profiling.Profiler.EndSample();

		// UnityEngine.Profiling.Profiler.BeginSample("Using object conversion");
		// Debug.LogError((object)string1);
		// UnityEngine.Profiling.Profiler.EndSample();



		Debug.LogError("string1 and string2 is equal:" + true);
		Debug.LogError("string1 and string2 is equal:" + (string1 == string2));


		string st = "we are in the end game now!" + true;
		Debug.LogError("st message:" + (object)st);

		if (string1 == string2)
		{
			Debug.LogError("string1 and string2 is equal");
		}

		Debug.LogError("string1 and string2 is equal:" + string1 == string2);


		Debug.LogError(" == : " + string1 == string2);
		print(" == : " + string1 == string2);

		Debug.LogError(" == : " + EqualsMe(string1, string2));
		Debug.LogError("reference equality:" + object.ReferenceEquals(string1, string2));
		Debug.LogError((System.Object)string1 + " " + (System.Object)string2);
		Debug.LogError((System.Object)string1 == (System.Object)string2);

		Debug.LogError("== :" + string3 == string4);
		Debug.LogError("equals:" + string3.Equals(string4));
		Debug.LogError("reference equality:" + object.ReferenceEquals(string3, string4));

		// ReferenceCheck();

	}

	static bool EqualsMe(string a, string b)
	{
		if ((System.Object)a == (System.Object)b)
		{
			return true;
		}

		if ((System.Object)a == null || (System.Object)b == null)
		{
			return false;
		}

		if (a.Length != b.Length)
			return false;

		throw new Exception("Not found");
	}
}
