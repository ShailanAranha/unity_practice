﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Ingredient {salt, water, spices};

public class IngredientUnit : MonoBehaviour {

	public string name;
	public Ingredient ingredient;
}
