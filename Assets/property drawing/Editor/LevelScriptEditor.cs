﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelScript))]
public class LevelScriptEditor : Editor {

	public override void OnInspectorGUI ()
	{
		//base.OnInspectorGUI ();

		DrawDefaultInspector ();

		LevelScript myLevelScript = (LevelScript)target;

		myLevelScript.experience = EditorGUILayout.IntField ("Experience", myLevelScript.experience);
		EditorGUILayout.LabelField ("Level", myLevelScript.level.ToString());




		if (GUILayout.Button ("Build")) {
			myLevelScript.build ();
		}
	}



}
