﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScript : MonoBehaviour {
	public int NoOfAttempts;
	public int experience;
	public GameObject obj;
	//for draw default insepctor
	public int Attempts;

	public int level {
		get { return experience / 750; }
		set { experience = value* 1000 ;}

	}

	public void build()
	{
		Instantiate (obj, new Vector3 (0, 0, 0), Quaternion.identity);
	}
}
