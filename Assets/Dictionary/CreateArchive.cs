﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateArchive {

	[MenuItem("Assets/Create/Archive Dictionary")]
	public static ArchiveDictionary Create()
	{
		ArchiveDictionary asset = ScriptableObject.CreateInstance<ArchiveDictionary> ();
		AssetDatabase.CreateAsset(asset, "Assets/Dictionary/ArchiveDictionary.asset");

		AssetDatabase.SaveAssets ();
		return asset;
	}
}
