﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ArchiveItems {


	public string ArchiveID= "ID";
	public string ArchiveName = "name";
	public string ArchiveType = "type";

	ArchiveItems(string id, string name, string type){
		this.ArchiveID = id;
		this.ArchiveName = name;
		this.ArchiveType = type;
		
	}


}
