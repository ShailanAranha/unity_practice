﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enum_test : MonoBehaviour,IEnumerator,IEnumerable
{

	// List<string> mylist = new List<string>(){"1","2","3","4","5"};
	List<string> mylist = new List<string>();

	IEnumerator<string> myenum;

	
	// Use this for initialization
	void Start()
	{
		test();

		IEnumerable enumRef = "text";
		
	
		// Debug.LogError(enumRef.GetType());

		//using is operator
		// if(enumRef is IEnumerable)
		// 	Debug.LogError(enumRef);

		if(enumRef.GetType()== typeof(string))
			Debug.LogError(enumRef);



		myenum = mylist.GetEnumerator();
		Debug.LogError(myenum.Current);
		myenum.MoveNext();
		myenum.MoveNext();
		myenum.MoveNext();
		myenum.MoveNext();

		myenum.Reset();

		myenum.MoveNext();
		myenum.MoveNext();

		Debug.LogError(myenum.Current);

		temp();


		//display all items of list by using string builder
		// StringBuilder br = new StringBuilder();

		// foreach (string item in mylist)
		// {
		// 	br.Append(item);
		// }

		// Debug.LogError("list:" + br + " ");

	}

	void temp()
	{
		myenum.MoveNext();
		myenum.MoveNext();

		myenum.MoveNext();
		myenum.MoveNext();

		Debug.LogError(myenum.Current);
	}

	// Update is called once per frame
	void Update()
	{

	}

	void test()
	{
		for (int i = 0; i < 5; i++)
		{
			mylist.Add(i.ToString());
		}

	}

	
	object IEnumerator.Current
	{
		get
		{
			
			throw new System.NotImplementedException();
		}
	}

	bool IEnumerator.MoveNext()
	{
		throw new System.NotImplementedException();
	}

	void IEnumerator.Reset()
	{
		throw new System.NotImplementedException();
	}

	// public IEnumerator GetEnumerator()
	// {
	// 	return ((IEnumerable)mylist).GetEnumerator();
	// }

	IEnumerator IEnumerable.GetEnumerator()
	{
		throw new System.NotImplementedException();
	}
}
