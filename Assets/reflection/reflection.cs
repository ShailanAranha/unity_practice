﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using System.Reflection;
using System;
using System.Reflection;

public class reflection : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{
		
		// GetTypeMethod();

		// TypeOfMethod();

		GetAllMethods();

		// MethodInfo myMethodInfo = myTypeObj.GetMethod("MyMethod");

		// Debug.Log("myTypeObj: "+myTypeObj.FullName+" myMethodInfo: "+myMethodInfo+" MyClassRef: "+MyClassRef);

	}

	void GetTypeMethod()
	{
		MyClass MyClassRef = new MyClass();
		MyClassRef.MyMethod();
		MyClass.myanothermethod();


		Type myTypeObj = MyClassRef.GetType();

		Debug.LogError("myTypeObj:" + myTypeObj.ToString());

	}


	void TypeOfMethod()
	{
		Type myClassProperties = typeof(MyClass);
		MethodInfo myMethodInfo = myClassProperties.GetMethod("MyMethod");
		Debug.LogError("myMethodInfo:" + myMethodInfo);
		// Debug.LogError("myTypeObj: "+myTypeObj.FullName+" myMethodInfo: "+myMethodInfo+" MyClassRef: "+MyClassRef);
		// Debug.LogError();
	}

	void GetAllMethods()
	{
		Type myClassProperties = typeof(MyClass);
		MethodInfo[] myMethodInfo = myClassProperties.GetMethods(BindingFlags.NonPublic | BindingFlags.Static|BindingFlags.Instance);

		// MethodInfo[] myMethodInfo = myClassProperties.GetMethods();


		foreach (var item in myMethodInfo)
		{
			Debug.LogError(item);
		}
	}

}

public class MyClass
{
	public static void myanothermethod()
	{
		Debug.LogError("my another method");
	}

	public void MyMethod()
	{
		Debug.LogError("my method");
	}

	private void MyMethod_private()
	{
		Debug.LogError("my private method ");
	}

	protected void MyMethod_protected()
	{
		Debug.LogError("my protected method");
	}

	internal void MyMethod_internal()
	{
		Debug.LogError("my internal method");
	}

	protected internal void MyMethod_protected_internal()
	{
		Debug.LogError("my protected internal method");
	}
}