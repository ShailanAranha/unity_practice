﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class eventListener : MonoBehaviour {

	UnityEvent myevent = new UnityEvent();
	// Use this for initialization
	void Start () {
		myevent.AddListener (MyAction);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			myevent.Invoke ();
		}
	}



	void MyAction()
	{
		gameObject.GetComponent<Renderer> ().material.color = Color.red;
	}
}
