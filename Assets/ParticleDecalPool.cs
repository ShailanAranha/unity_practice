﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDecalPool : MonoBehaviour {


	private int particleDecalDataIndex ;
	public int maxDecals = 100;

	public float maxDecalSize= 1.5f;
	public float minDecalsize = 1f;

	//private particleDecalData[] particleData;

	private particleDecalData[] particleData;
	private ParticleSystem.Particle[] particles;

	public ParticleSystem DecalParticelSystem;

	// Use this for initialization
	void Start () {
		DecalParticelSystem = GetComponent<ParticleSystem> ();
		particles = new ParticleSystem.Particle[maxDecals];
		particleData = new particleDecalData[maxDecals];
		for (int i = 0; i < maxDecals; i++) {
			particleData [i] = new particleDecalData ();
		}
	}

	public void HitParticle(ParticleCollisionEvent particleCollisionEvent, Gradient color)
	{
		SetParticledata( particleCollisionEvent ,color);
		DisplayParticle ();
	}
	
	void SetParticledata(ParticleCollisionEvent particleCollisionEvent, Gradient color)
	{
		if (particleDecalDataIndex >= maxDecals) {
			particleDecalDataIndex = 0;
		}

		particleData [particleDecalDataIndex].position = particleCollisionEvent.intersection;

		Vector3 particleDecalRotation = Quaternion.LookRotation (particleCollisionEvent.normal).eulerAngles;

		particleDecalRotation.z = Random.Range (0, 360);

		particleData [particleDecalDataIndex].rotation = particleDecalRotation;

		particleData [particleDecalDataIndex].size = Random.Range (minDecalsize, maxDecalSize);

		particleData [particleDecalDataIndex].color = color.Evaluate (Random.Range (0f, 1f));

		particleDecalDataIndex++;
	}

	void DisplayParticle()
	{
		for (int i = 0; i < particleData.Length; i++) {

			particles [i].position = particleData [i].position;
			particles [i].rotation3D = particleData [i].rotation;
			particles [i].startSize = particleData [i].size;
			particles [i].startColor = particleData [i].color;
		}

		DecalParticelSystem.SetParticles (particles, particles.Length);
	}
}
