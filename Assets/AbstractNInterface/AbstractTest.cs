﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbstractTest : MonoBehaviour
{
	private void Start()
	{
		DerivedClass m_obj = new DerivedClass("Avenger", "Tony stark", "New York");

		Debug.LogError(m_obj.Id + m_obj.Name + m_obj.Location);
	}

}

public abstract class BaseClass
{
	public readonly string Id = null;
	public readonly string Name = null;

	public BaseClass(string a_strId, string a_strName)
	{
		Id = a_strId;
		Name = a_strName;
	}

}

public class DerivedClass : BaseClass
{
	public readonly string Location = null;

	public DerivedClass(string a_strId, string a_strName, string a_location) : base(a_strId, a_strName)
	{
		Location = a_location;
	}
}

